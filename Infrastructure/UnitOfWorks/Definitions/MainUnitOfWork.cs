﻿using Infrastructure.Data;
using Infrastructure.Repositorys.Contracts;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Base.Definitions;

namespace Infrastructure.UnitOfWorks.Definitions;

public class MainUnitOfWork : UnitOfWorkBase, IMainUnitOfWork
{
    public IUserRepository User { get; }

    public ISessionRepository Session { get; }

    public IProjectRepository Project { get; }

    public ITaskRepository Task { get; }

    public INotificationRepository Notification { get; }

    public MainUnitOfWork(
        DataContext context,
        IUserRepository userRepository,
        ISessionRepository sessionRepository,
        IProjectRepository projectRepository,
        ITaskRepository taskRepository,
        INotificationRepository notificationRepository
    ) : base(context)
    {
        User = userRepository;
        Session = sessionRepository;
        Project = projectRepository;
        Task = taskRepository;
        Notification = notificationRepository;
    }
}
