﻿using Infrastructure.Repositorys.Contracts;
using Kernel.Base.Contracts;

namespace Infrastructure.UnitOfWorks.Contracts;

public interface IMainUnitOfWork : IUnitOfWorkBase
{
    IUserRepository User { get; }
    ISessionRepository Session { get; }
    IProjectRepository Project { get; }
    ITaskRepository Task { get; }
    INotificationRepository Notification { get; }
}
