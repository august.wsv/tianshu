﻿using Domain.Models;

namespace Infrastructure.Data;

public class DataInitializer
{
    public static void Seed(DataContext context)
    {
        context.Database.EnsureCreated();

        if (!context.Users.Any())
        {
            List<User> data =
            [
                new User
                {
                    FullName = "秦始皇",
                    Username = "qinshihuang",
                    EncryptedPassword = BCrypt.Net.BCrypt.HashPassword("88888888"),
                    SecretKeyFor2FA = "6XNJMCNBQQJYHZIHDQERA5D6I6UEHFGA"
                },
                new User
                {
                    FullName = "Test",
                    Username = "test_1",
                    EncryptedPassword = BCrypt.Net.BCrypt.HashPassword("88888888"),
                    SecretKeyFor2FA = "VYEQGVKPWVSUOGI5JVYZN2OT23HIB6NX"
                }
            ];
            context.Users.AddRange(data);
            context.SaveChanges();
        }
    }
}
