﻿using Infrastructure.Repositorys.Contracts;
using Infrastructure.Repositorys.Definitions;
using Infrastructure.UnitOfWorks.Contracts;
using Infrastructure.UnitOfWorks.Definitions;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Providers;

public static class InfrastructureRegisterServiceCollectionExtensions
{
    public static IServiceCollection AddInfrastructureRegister(this IServiceCollection services)
    {
        //@Register UnitOfWork
        services
            //.AddScoped<IExampleUoW, ExampleUoW>()
            .AddScoped<IMainUnitOfWork, MainUnitOfWork>();

        //@Register Repositories
        services
            //.AddScoped<IExampleRepository, ExampleRepository>()
            .AddScoped<ISessionRepository, SessionRepository>()
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<IProjectRepository, ProjectRepository>()
            .AddScoped<ITaskRepository, TaskRepository>()
            .AddScoped<INotificationRepository, NotificationRepository>();
        return services;
    }
}
