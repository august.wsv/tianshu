﻿using Domain.Models;
using Kernel.Base.Contracts.IRepositoryBase;

namespace Infrastructure.Repositorys.Contracts;

public interface ITaskRepository : IRepositoryBaseIdIsNumber<TaskX> { }
