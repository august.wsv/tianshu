﻿using Domain.Models;
using Kernel.Base.Contracts.IRepositoryBase;

namespace Infrastructure.Repositorys.Contracts;

public interface ISessionRepository : IRepositoryBaseIdIsLetter<Session> { }
