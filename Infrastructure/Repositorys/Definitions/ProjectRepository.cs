﻿using Domain.Models;
using Infrastructure.Data;
using Infrastructure.Repositorys.Contracts;
using Kernel.Base.Definitions.RepositoryBase;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Repositorys.Definitions;

public class ProjectRepository : RepositoryBaseIdIsNumber<Project>, IProjectRepository
{
    public ProjectRepository(DataContext context, IMemoryCache cache) : base(context, cache) { }
}
