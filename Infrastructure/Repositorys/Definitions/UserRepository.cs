﻿using Domain.Models;
using Infrastructure.Data;
using Infrastructure.Repositorys.Contracts;
using Kernel.Base.Definitions.RepositoryBase;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Repositorys.Definitions;

public class UserRepository : RepositoryBaseIdIsNumber<User>, IUserRepository
{
    public UserRepository(DataContext context, IMemoryCache cache) : base(context, cache) { }
}
