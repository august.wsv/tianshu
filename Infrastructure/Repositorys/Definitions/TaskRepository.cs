﻿using Domain.Models;
using Infrastructure.Data;
using Infrastructure.Repositorys.Contracts;
using Kernel.Base.Definitions.RepositoryBase;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Repositorys.Definitions;

public class TaskRepository : RepositoryBaseIdIsNumber<TaskX>, ITaskRepository
{
    public TaskRepository(DataContext context, IMemoryCache cache) : base(context, cache) { }
}
