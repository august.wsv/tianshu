﻿using Application.Requests;
using Application.Responses;
using Application.Services.Contracts;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Support.Contracts;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Otp;

namespace Application.Services.Definitions;

public class CommonService : ICommonService
{
    private readonly IMainUnitOfWork _mainUnitOfWork;

    private readonly IHelper _helper;

    private readonly IConfiguration _configuration;

    private readonly IMemoryCache _cache;

    public CommonService(
        IMainUnitOfWork mainUnitOfWork, 
        IHelper helper, 
        IConfiguration configuration, 
        IMemoryCache cache
    )
    {
        _mainUnitOfWork = mainUnitOfWork;
        _helper = helper;
        _configuration = configuration;
        _cache = cache;
    }


    public dynamic GetJwtConfig()
    {
        object config = new
        {
            Issuer = _configuration["JWT:Issuer"],
            Audience = _configuration["JWT:Audience"],
            Secret = _configuration["JWT:Secret"]
        };
        return _helper.Respond(data: config);
    }

    public dynamic ConfirmForgottenPassword(UserRequest.ConfirmForgottenPassword userReqConfirmForgottenPassword)
    {
        UserResponse.FindYourAccount? userResFindYourAccountCacheValue = _cache.Get<UserResponse.FindYourAccount>(userReqConfirmForgottenPassword.FindYourAccountCacheKey);
        if (userResFindYourAccountCacheValue == null)
        {
            return _helper.Respond(status: 401, message: "Not found userResFindYourAccountCacheKey");
        }
        string otpCode = new NextOtp(secretKey: Base32Encoding.ToBytes(userResFindYourAccountCacheValue.SecretKeyFor2FA), step: 30, mode: HashMode.Sha1, totpSize: 12).ComputeOtp();
        Console.WriteLine(otpCode);
        if (otpCode == userReqConfirmForgottenPassword.NextOtpCode)
        {
            _cache.Remove(userReqConfirmForgottenPassword.FindYourAccountCacheKey);
            string completePasswordChangeCacheKey = $"CompletePasswordChange_{userResFindYourAccountCacheValue.Username}";
            _cache.Set(
                key: completePasswordChangeCacheKey,
                value: true,
                options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(5))
            );
            return _helper.Respond(message: "You can change password for 5 minutes!");
        }
        return _helper.Respond(status: 401, message: "The information you provided is incorrect");
    }

}
