﻿using Application.Services.Contracts;
using Application.UtilityClasses;
using Domain.Models;
using Infrastructure.UnitOfWorks.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System.Net.WebSockets;
using System.Text;

namespace Application.Services.Definitions;

public class AuthService : IAuthService
{
    private readonly IMainUnitOfWork _mainUnitOfWork;

    private readonly IMemoryCache _cache;

    public AuthService(IMainUnitOfWork mainUnitOfWork, IMemoryCache cache)
    {
        _mainUnitOfWork = mainUnitOfWork;
        _cache = cache;
    }

    private async Task<bool> OnConnecting(HttpContext context)
    {
        Console.WriteLine("Connected");

        string sessionId = context.Request.Query["session_id"].ToString() ?? "";
        SessonLive sessonLiveCheck = new()
        {
            SessonId = sessionId,
            IpAddress = context.Connection.RemoteIpAddress?.ToString() ?? "_FAKE_",
            BrowserType = context.Request.Headers.UserAgent.ToString()
        };

        SessonLive? sessonLive = StaticValue.sessionLives.FirstOrDefault(x => x.SessonId.Equals(sessonLiveCheck.SessonId));

        if (sessonLive is not null)
        {
            if (!sessonLive.IpAddress.Equals(sessonLiveCheck.IpAddress) || !sessonLive.BrowserType.Equals(sessonLiveCheck.BrowserType))
            {
                Console.WriteLine("WARRNING");
                Session? session = await _mainUnitOfWork.Session.GetObjectById<Session>(sessonLive.SessonId);
                if (session is not null)
                {
                    _ = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
                    {
                        await _mainUnitOfWork.Session.Delete(session);
                    });
                    StaticValue.sessionLives.Remove(sessonLive);
                    return false;
                }
            }
            else
            {
                sessonLive.ConnIds.Add(context.Connection.Id);
                return true;
            }
        }
        else if (sessionId != "null")
        {
            sessonLiveCheck.ConnIds.Add(context.Connection.Id);
            StaticValue.sessionLives.Add(sessonLiveCheck);
            Session? session = await _mainUnitOfWork.Session.GetObjectById<Session>(sessonLiveCheck.SessonId);
            if (session is not null)
            {
                session.IpAddress = sessonLiveCheck.IpAddress;
                session.BrowserType = sessonLiveCheck.BrowserType;
                return await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
                {
                    await _mainUnitOfWork.Session.Update(session);
                });
            }
        }
        return false;
    }

    public async Task OnConnected(HttpContext context)
    {
        bool statusOnConnected = await OnConnecting(context);
        if (statusOnConnected)
        {
            using WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
            try
            {
                byte[] buffer = new byte[1024 * 4];
                WebSocketReceiveResult receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                while (!receiveResult.CloseStatus.HasValue)
                    receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }
            finally
            {
                await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by the server", CancellationToken.None);
            }
        }
    }

    public async Task OnDisconnected(HttpContext context)
    {
        string connectId = context.Connection.Id;
        Console.WriteLine($"Disconnected {connectId}");
        SessonLive? foundLoging = StaticValue.sessionLives.FirstOrDefault(loging => loging.ConnIds.Contains(connectId));
        if (foundLoging is not null)
        {
            foundLoging.ConnIds.Remove(connectId);
            if (foundLoging?.ConnIds.Count == 0)
            {
                Session? session = await _mainUnitOfWork.Session.GetObjectById<Session>(foundLoging.SessonId);
                if (session is not null)
                {
                    session.IpAddress = null;
                    session.BrowserType = null;
                    bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
                    {
                        await _mainUnitOfWork.Session.Update(session);
                    });
                }                
                StaticValue.sessionLives.Remove(foundLoging);
            }
        }
        await Task.CompletedTask;
    }

    public async Task OnCompleteRegister(HttpContext context)
    {
        string userCacheKey = context.Request.Query["user_cache_key"].ToString() ?? "";
        User? userCache = _cache.Get<User>(userCacheKey);
        if (userCache is not null)
        {            
            using WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
            CompleteRegisterLive completeRegisterLive =  new () { CacheKey = userCacheKey, Status = false };
            try
            {
                StaticValue.completeRegisterLives.Add(completeRegisterLive);
                byte[] buffer = new byte[1024 * 4];

                while (webSocket.State == WebSocketState.Open)
                {
                    if (completeRegisterLive.Status)
                    {
                        string message = "Register account susscess!";
                        byte[] messageBuffer = Encoding.UTF8.GetBytes(message);
                        await webSocket.SendAsync(new ArraySegment<byte>(messageBuffer), WebSocketMessageType.Text, true, CancellationToken.None);
                        break;
                    }
                    await Task.Delay(100);
                }
            }
            finally
            {
                await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by the server", CancellationToken.None);
                StaticValue.completeRegisterLives.Remove(completeRegisterLive);
            }
        }
    }
}
