﻿using Application.Constants;
using Application.Requests;
using Application.Responses;
using Application.Services.Contracts;
using Application.UtilityClasses;
using AutoMapper;
using Domain.Models;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Constants;
using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Otp;
using System.IdentityModel.Tokens.Jwt;

namespace Application.Services.Definitions;

public class UserService : IUserService
{
    private readonly IMemoryCache _cache;
    private readonly IMainUnitOfWork _mainUnitOfWork;
    private readonly IMapper _mapper;
    private readonly IHelper _helper;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public UserService(
        IMemoryCache cache,
        IMainUnitOfWork mainUnitOfWork,
        IMapper mapper,
        IHelper helpers,
        IHttpContextAccessor httpContextAccessor
    )
    {
        _cache = cache;
        _mainUnitOfWork = mainUnitOfWork;
        _mapper = mapper;
        _helper = helpers;
        _httpContextAccessor = httpContextAccessor;
    }

    public async Task<dynamic> Login(UserRequest.Login userReqLogin)
    {
        string userLoginLockedCacheKey = $"User_Login_{userReqLogin.Username}_Locked";
        if (_cache.TryGetValue(userLoginLockedCacheKey, out _))
        {
            return _helper.Respond(status: 401, message: "Too many failed attempts. Account is locked for 1 hour.");
        }
        UserResponse.Login? userResLogin = await _mainUnitOfWork.User.GetObjectByField(
            predicate: user => user.Username == userReqLogin.Username,
            queryModifier: query => query
                .Select(user => new UserResponse.Login
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username,
                    EncryptedPassword = user.EncryptedPassword,
                    SecretKeyFor2FA = user.SecretKeyFor2FA
                }
            ),
            itemStatus: ItemStatus.All
        );
        if (userResLogin is not null)
        {
            bool passwordStatus = BCrypt.Net.BCrypt.Verify(userReqLogin.Password, userResLogin.EncryptedPassword);
            string otpCode = new NextOtp(secretKey: Base32Encoding.ToBytes(userResLogin.SecretKeyFor2FA), step: 30, mode: HashMode.Sha1, totpSize: 12).ComputeOtp();
            Console.WriteLine(otpCode);
            bool otpStatus = userReqLogin.NextOtpCode == otpCode;
            string userLoginCountCacheKey = $"User_Login_{userReqLogin.Username}_Count";
            if (passwordStatus && otpStatus)
            {
                int personType = (int)PersonType.User;
                long personId = userResLogin.Id;
                Notification notification = new()
                {
                    Id = $"{DateTime.UtcNow.Ticks}.{personType}.{_helper.ConvertToBase26(personId)}",
                    PersonType = personType,
                    PersonId = personId,
                    Type = 1, //From system
                    KeyValuePairs = new()
                        {
                            { "Level", 1 },
                            { "DeviceType", _httpContextAccessor?.HttpContext?.Request.Headers.UserAgent.ToString() ?? "_Unknown_" }
                        }
                };
                await SendNotificationToUser(notification);
                _cache.Remove(userLoginCountCacheKey);
                return await LoginComfirmed(userResLogin);
            }
            UserLoginCountCacheValue? userLoginCountCacheValue = _cache.Get<UserLoginCountCacheValue>(userLoginCountCacheKey);
            if (userLoginCountCacheValue == null)
            {
                UserLoginCountCacheValue userLoginCountCacheValueInit = new UserLoginCountCacheValue
                {
                    Count = 1,
                    ExpiredTo = DateTimeOffset.UtcNow.AddMinutes(10),
                };
                _cache.Set(
                    key: userLoginCountCacheKey,
                    value: userLoginCountCacheValueInit,
                    options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(userLoginCountCacheValueInit.ExpiredTo)
                );
            }
            else
            {
                UserLoginCountCacheValue userLoginCountCacheValueChange = new()
                {
                    Count = ++userLoginCountCacheValue.Count,
                    ExpiredTo = userLoginCountCacheValue.ExpiredTo,
                };
                _cache.Set(
                    key: userLoginCountCacheKey,
                    value: userLoginCountCacheValueChange,
                    options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(userLoginCountCacheValue.ExpiredTo)
                );
                if (userLoginCountCacheValue.Count == 5)
                {
                    _cache.Remove(userLoginCountCacheKey);
                    _cache.Set(
                        key: userLoginLockedCacheKey,
                        value: true,
                        options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddHours(1))
                    );
                    int personType = (int)PersonType.User;
                    long personId = userResLogin.Id;
                    Notification notification = new()
                    {
                        Id = $"{DateTime.UtcNow.Ticks}.{personType}.{_helper.ConvertToBase26(personId)}",
                        PersonType = personType,
                        PersonId = personId,
                        Type = 1, //From system
                        KeyValuePairs = new()
                        {
                            { "Level", 2 },
                            { "IPAddress", _httpContextAccessor?.HttpContext?.Connection.RemoteIpAddress?.ToString() ?? "_Unknown_" },
                            { "DeviceType", _httpContextAccessor?.HttpContext?.Request.Headers.UserAgent.ToString() ?? "_Unknown_" }
                        }
                    };
                    await SendNotificationToUser(notification);
                }
            }
            if (passwordStatus && !otpStatus)
            {
                return _helper.Respond(status: 401, message: "NextOtpCode is not correct");
            }
        }
        return _helper.Respond(status: 401, message: "Username or Password is not correct");
    }

    public async Task<dynamic> GetToken(UserRequest.GetToken userReqGetToken)
    {
        Session? session = await _mainUnitOfWork.Session.GetObjectById<Session>(userReqGetToken.SessionId);
        if (session is not null)
        {
            DateTime dateTimeNow = DateTime.UtcNow;
            DateTime dateTimeNextWeek = dateTimeNow.AddDays(7);
            if (session.ExpiredTo <= dateTimeNow.Ticks)
            {
                long personId = session.PersonId;
                int personType = (int)PersonType.User;
                string token = new JwtSecurityTokenHandler().WriteToken(_helper.GetJwtTokenRsaSha256(
                    authClaim: [new("person-type", personType.ToString()), new("person-id", personId.ToString())],
                    time: dateTimeNextWeek
                ));
                session.TokenHash = _helper.ComputeHashJwt(token);
                session.ExpiredTo = dateTimeNextWeek.Ticks;
                bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
                {
                    await _mainUnitOfWork.Session.Update(session);
                });

                return status
                    ? _helper.Respond(metadata: new { accessToken = token })
                    : _helper.Respond(status: 401, message: "Error!");
            }
            else
            {
                return _helper.Respond(status: 401, message: "It's not time yet!");
            }
        }
        return _helper.Respond(status: 401, message: "Not found to logout!");
    }

    public async Task<dynamic> Logout(UserRequest.Logout userReqLogout)
    {
        Session? session = await _mainUnitOfWork.Session.GetObjectById<Session>(userReqLogout.SessionId);
        if (session is not null)
        {
            bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
            {
                await _mainUnitOfWork.Session.Delete(session);
            });
            return status ?
                _helper.Respond(message: "Success to logout!") :
                _helper.Respond(status: 500, message: "Error because server!");
        }
        return _helper.Respond(status: 401, message: "Not found to logout!");
    }

    public async Task<dynamic> PrepareRegister(UserRequest.PrepareRegister userReqPrepareRegister)
    {
        UserResponse.PrepareRegister? userResPrepareRegister = await _mainUnitOfWork.User.GetObjectByField(
            predicate: user => user.Username == userReqPrepareRegister.Username,
            queryModifier: query => query
                .Select(user => new UserResponse.PrepareRegister
                {
                    Username = user.Username
                }
            ),
            itemStatus: ItemStatus.All
        );

        if (userResPrepareRegister == null)
        {
            string base32Secret = Base32Encoding.ToString(KeyGeneration.GenerateRandomKey(20));
            User user = _mapper.Map<User>(userReqPrepareRegister);
            user.EncryptedPassword = BCrypt.Net.BCrypt.HashPassword(userReqPrepareRegister.Password);
            user.SecretKeyFor2FA = base32Secret;
            string userPrepareRegisterCacheKey = $"User_PrepareRegister_{userReqPrepareRegister.Username}";
            _cache.Set(
                key: userPrepareRegisterCacheKey,
                value: user,
                options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(15))
            );
            string otpAuthUrl = $"otpauth://nextotp?app={"TianShu"}&username={user.Username}&secret={base32Secret}&user_prepare_register_cache_key={userPrepareRegisterCacheKey}";
            object metadata = new
            {
                userPrepareRegisterCacheKey,
                otpAuthUrl,
            };

            return _helper.Respond(message: "Waiting for complete register!", metadata: metadata);
        }
        return _helper.Respond(status: 401, message: "Username already exists!");
    }

    public async Task<dynamic> CompleteRegister(UserRequest.CompleteRegister userReqCompleteRegister)
    {
        User? userCache = _cache.Get<User>(userReqCompleteRegister.UserCacheKey);
        if (userCache == null)
        {
            return _helper.Respond(status: 401, message: "Failed to create account!");
        }
        byte[] secretKey = Base32Encoding.ToBytes(userCache.SecretKeyFor2FA);
        string otpCode = new NextOtp(secretKey, step: 30, mode: HashMode.Sha1, totpSize: 12).ComputeOtp();
        Console.WriteLine(otpCode);
        if (!StaticValue.completeRegisterLives.Any(cr => cr.CacheKey == userReqCompleteRegister.UserCacheKey))
        {
            return _helper.Respond(status: 401, message: "Authenticator app not connect!");
        }
        if (userReqCompleteRegister.NextOtpCode != otpCode)
        {
            return _helper.Respond(status: 401, message: "NextOtpCode not right!");
        }
        bool statusCreate = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.User.Create(userCache);
        });
        if (statusCreate)
        {
            CompleteRegisterLive? completeRegisterLive = StaticValue.completeRegisterLives.FirstOrDefault(crl => crl.CacheKey == userReqCompleteRegister.UserCacheKey);
            if (completeRegisterLive != null)
            {
                completeRegisterLive.Status = true;
            }
            _cache.Remove(userReqCompleteRegister.UserCacheKey);
            if (userReqCompleteRegister.WillLogin == true)
            {
                UserResponse.Login? userResLogin = await _mainUnitOfWork.User.GetObjectByField(
                    predicate: user => user.Username == userCache.Username,
                    queryModifier: query => query
                        .Select(user => new UserResponse.Login
                        {
                            Id = user.Id,
                            FullName = user.FullName,
                            Username = user.Username,
                            EncryptedPassword = user.EncryptedPassword,
                            SecretKeyFor2FA = user.SecretKeyFor2FA
                        }
                    ),
                    itemStatus: ItemStatus.All
                );
                return userResLogin is not null
                    ? await LoginComfirmed(userResLogin)
                    : _helper.Respond(status: 401, message: "Wrong to find User!");
            }
        }
        return _helper.Respond(message: $"Susscess: {statusCreate}");
    }

    public async Task<dynamic> RemoveAccount(UserRequest.RemoveAccount userReqRemoveAccount)
    {
        string? personIdString = _httpContextAccessor.HttpContext?.User?.FindFirst("person-id")?.Value;
        string? personTypeString = _httpContextAccessor.HttpContext?.User?.FindFirst("person-type")?.Value;
        if (personIdString == null || personTypeString == null)
        {
            return _helper.Respond(status: 401, message: "Something was wrong");
        }
        int personType = int.Parse(personTypeString);
        if ((PersonType)personType != PersonType.User)
        {
            return _helper.Respond(status: 401, message: "You are not user!");
        }
        int userId = int.Parse(personIdString);
        User? user = await _mainUnitOfWork.User.GetObjectById<User>(userId);
        if (user == null)
        {
            return _helper.Respond(status: 401, message: "Not found user in db");
        }
        bool passwordStatus = BCrypt.Net.BCrypt.Verify(userReqRemoveAccount.Password, user.EncryptedPassword);
        string otpCode = new NextOtp(secretKey: Base32Encoding.ToBytes(user.SecretKeyFor2FA), step: 30, mode: HashMode.Sha1, totpSize: 12).ComputeOtp();
        Console.WriteLine(otpCode);
        bool otpStatus = userReqRemoveAccount.NextOtpCode == otpCode;
        if (!passwordStatus || !otpStatus)
        {
            return _helper.Respond(status: 401, message: "Password or NextOtpCode is not correct");
        }
        List<Session> sessions = await _mainUnitOfWork.Session.GetList(
            queryModifier: query => query
                .Where(session =>
                    session.PersonId.Equals(user.Id) &&
                    session.PersonType.Equals((int)PersonType.User)
                )
        );
        List<Notification> notifications = await _mainUnitOfWork.Notification.GetList(
             queryModifier: query => query
                 .Where(notification =>
                     notification.PersonId.Equals(user.Id) &&
                     notification.PersonType.Equals((int)PersonType.User)
                 )
        );
        bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.User.Delete(user);
            await _mainUnitOfWork.Session.Delete(sessions);
            await _mainUnitOfWork.Notification.Delete(notifications);
        });
        if (status) { /*TODO: Dùng Kafka để xóa các thông tin của user ở các dịch vụ con */ }
        return _helper.Respond(message: $"Remove account status: {status}");
    }

    public async Task<dynamic> FindYourAccount(UserRequest.FindYourAccount userReqFindYourAccount)
    {
        UserResponse.FindYourAccount? userResFindYourAccount = await _mainUnitOfWork.User.GetObjectByField(
            predicate: user => user.Username == userReqFindYourAccount.Username,
            queryModifier: query => query
                .Select(user => new UserResponse.FindYourAccount
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username,
                    CreatedAt = user.CreatedAt,
                    SecretKeyFor2FA = user.SecretKeyFor2FA
                }
            ),
            itemStatus: ItemStatus.All
        );

        if(userResFindYourAccount != null)
        {
            DateTime CreatedAt12hAfter = userResFindYourAccount.CreatedAt.AddHours(12);
            DateTime CreatedAt12hBefore = userResFindYourAccount.CreatedAt.AddHours(-12);
            DateTime? RegisterDateTime = userReqFindYourAccount.RegisterDate?.ToDateTime(TimeOnly.MinValue);
            if (RegisterDateTime >= CreatedAt12hBefore && RegisterDateTime <= CreatedAt12hAfter) 
            {
                int personType = (int)PersonType.User;
                long personId = userResFindYourAccount.Id;
                string userConfirmForgottenPasswordCacheKey = $"User_ConfirmForgottenPassword_{userResFindYourAccount.Username}";
                Notification notification = new()
                {
                    Id = $"{DateTime.UtcNow.Ticks}.{personType}.{_helper.ConvertToBase26(personId)}",
                    PersonType = personType,
                    PersonId = personId,
                    Type = 1, //From system
                    KeyValuePairs = new()
                    {
                        { "Level", 3 },
                        { "DeviceType", _httpContextAccessor?.HttpContext?.Request.Headers.UserAgent.ToString() ?? "_Unknown_" },
                        { "User_ConfirmForgottenPassword_CacheKey", userConfirmForgottenPasswordCacheKey }
                    }
                };                
                _cache.Set(
                    key: userConfirmForgottenPasswordCacheKey,
                    value: userResFindYourAccount,
                    options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(5))
                );
                await SendNotificationToUser(notification);
                string message = "We have sent a notification requesting to change your password, please click yes to continue";
                object metadata = new
                {
                    UwcPassword = userResFindYourAccount.Username,
                };
                return _helper.Respond(message: message, metadata: metadata);
            }        
        }
        return _helper.Respond(status: 401, message: "The information you provided is incorrect");
    }

    public async Task<dynamic> CompletePasswordChange(UserRequest.CompletePasswordChange userReqCompletePasswordChange)
    {
        string userConfirmForgottenPasswordCacheKey = $"User_ConfirmForgottenPassword_{userReqCompletePasswordChange.Username}";
        if (!_cache.TryGetValue(userConfirmForgottenPasswordCacheKey, out _))
        {
            return _helper.Respond(status: 401, message: "QUas thoi han Cache.");
        }
        User? user = await _mainUnitOfWork.User.GetObjectByField<User>(
            predicate: user => user.Username == userReqCompletePasswordChange.Username
        );
        if (user == null)
        {
            return _helper.Respond(status: 404, message: "Not found user");
        }
        user.EncryptedPassword = BCrypt.Net.BCrypt.HashPassword(userReqCompletePasswordChange.Password);
        bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.User.Update(user);
        });
        return _helper.Respond(message: status.ToString());
    }

    private async Task<dynamic> LoginComfirmed(UserResponse.Login userResLogin)
    {
        int personType = (int)PersonType.User;
        long personId = userResLogin.Id;
        DateTime dateTimeNow = DateTime.UtcNow;
        DateTime dateTimeNextWeek = dateTimeNow.AddDays(7);
        string token = new JwtSecurityTokenHandler().WriteToken(_helper.GetJwtTokenRsaSha256(
            authClaim: [new("person-type", personType.ToString()), new("person-id", personId.ToString())],
            time: dateTimeNextWeek
        ));
        Session session = new()
        {
            Id = $"{dateTimeNow.Ticks}.{personType}.{_helper.ConvertToBase26(personId)}",
            TokenHash = _helper.ComputeHashJwt(token),
            PersonType = personType,
            PersonId = personId,
            ExpiredTo = dateTimeNextWeek.Ticks,
        };
        object metadata = new
        {
            accessToken = token,
            sessionId = session.Id,
        };
        bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.Session.Create(session);
        });

        UserResponse.ShowPublic userResShowPublic = new UserResponse.ShowPublic
        {
            Username = userResLogin.Username,
            FullName = userResLogin.FullName,
        };

        return status
            ? _helper.Respond(data: userResShowPublic, metadata: metadata)
            : _helper.Respond(status: 500, message: "Can't login!");
    }

    private async Task SendNotificationToUser(Notification notification)
    {
        await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.Notification.Create(notification);
        });
        /*TODO: Sử dụng SignalR để thông báo đến đến điện thoại*/
    }
}
