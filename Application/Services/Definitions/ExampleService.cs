﻿using Application.Requests;
using Application.Responses;
using Application.Services.Contracts;
using AutoMapper;
using Domain.Models;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Constants;
using Kernel.Support.Contracts;
using Kernel.UtilityClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace Application.Services.Definitions;

public class ExampleService : IExampleService
{
    private readonly IMainUnitOfWork _mainUnitOfWork;

    private readonly IHelper _helper;

    private readonly IMapper _mapper;

    public ExampleService(IMainUnitOfWork mainUnitOfWork, IHelper helper, IMapper mapper)
    {
        _mainUnitOfWork = mainUnitOfWork;
        _helper = helper;
        _mapper = mapper;
    }

    public async Task<dynamic> Query(string? username, int page = 1, int pageSize = 10)
    {
        var getPagedListCache = await _mainUnitOfWork.User.GetPagedListCache(
            cacheKey: "4B57F943-31B5-463B-BB40-8518BBCB6B9B",
            page: page,
            pageSize: pageSize,
            querySelect: query => query
                .Select(user => new UserResponse.ForCache
                {
                    FullName = user.FullName,
                    Username = user.Username,
                    DeletedAt = user.DeletedAt
                }),
            queryModifier: query => query
                .Where(user => string.IsNullOrEmpty(username) || user.Username.Contains(username)),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var getPagedList = await _mainUnitOfWork.User.GetPagedList(
            page: page,
            pageSize: pageSize,
            queryModifier: query => query
                .Where(user => string.IsNullOrEmpty(username) || user.Username.Contains(username))
                .Select(user => new UserResponse.ShowPublicII
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username
                }),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var getList = await _mainUnitOfWork.User.GetList(
            queryModifier: query => query
                .Where(user => string.IsNullOrEmpty(username) || user.Username.Contains(username))
                .Select(user => new UserResponse.ShowPublicII
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username
                }),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var getObjectById = await _mainUnitOfWork.User.GetObjectById(
            id: 1,
            queryModifier: query => query
                .Select(user => new UserResponse.ShowPublicII
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username
                }),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var getCount = await _mainUnitOfWork.User.GetCount(
            predicate: user => user.Username.Contains(username),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var satisfyAny = await _mainUnitOfWork.User.SatisfyAny(
            predicate: user => user.Id == 2,
            queryModifier: query => query.Where(user => user.Username.Contains(username)),
            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var satisfyAll = await _mainUnitOfWork.User.SatisfyAll(
            predicate: user => user.Id == 2,
            queryModifier: query => query.Where(user => user.Username.Contains(username)),

            itemStatus: ItemStatus.OnlyNotTrash
        );

        //______________________________________________________________________________________________________________________________________

        var data = new
        {
            getPagedListCache,
            getPagedList,
            getList,
            getObjectById,
            getCount,
            satisfyAny,
            satisfyAll
        };

        var metadata = new
        {
            metadata1 = _helper.GetInfiniteScrollMetadata(getPagedListCache, page, pageSize),
            metadata2 = _helper.GetPaginationMetadata(getPagedListCache, page, pageSize)
        };
        return _helper.Respond(data: data, metadata: metadata);
    }

    public async Task<dynamic> QueryII()
    {
        var data1 = await _mainUnitOfWork.Project.GetList(
            queryModifier: query => query.AsNoTracking()
                .Include(project => project.Tasks)
                .Select(project => new ProjectResponse
                {
                    Id = project.Id,
                    Name = project.Name,
                    Tasks = project.Tasks.Select(task => new ProjectResponse.WTaskX
                    {
                        Id = task.Id,
                        Name = task.Name,
                        Deadline = task.Deadline,
                    }).ToList()
                })
                ,
            itemStatus: ItemStatus.All
        );
        //______________________________________________________________________________________________________________________________________

        var data2 = await _mainUnitOfWork.Task.GetObjectById(
            id: 9,
            queryModifier: query => query
                .Include(task => task.Project)
                .Select(task => new TaskResponse
                {
                    Id = task.Id,
                    Name = task.Name,
                    Deadline = task.Deadline,
                    Project = new TaskResponse.WProject
                    {
                        Id = task.Project.Id,
                        Name = task.Project.Name
                    }
                }
            )
        );
        //______________________________________________________________________________________________________________________________________

        var userResLogin = await _mainUnitOfWork.User.GetObjectByField(
            predicate: user => user.Username.Equals("qinshihuang"),
            queryModifier: query => query
                .Select(user => new UserResponse.Login
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    Username = user.Username,
                    EncryptedPassword = user.EncryptedPassword,
                    SecretKeyFor2FA = user.SecretKeyFor2FA
                }
            ),
            itemStatus: ItemStatus.All
        );

        var data = new
        {
            data1,
            data2,
            userResLogin
        };
        return _helper.Respond(data: data);
    }

    public async Task<dynamic> Command(CancellationToken cancellationToken)
    {
        List<Project> projects =
        [
            new()
            {
                Name = "Nettimer",
                Customer = "Xiv Digital",
                StartDate = new DateOnly(2024, 10, 21),
                EndDate = null,
            },
            new()
            {
                Name = "Hyper Assistant",
                Customer = "ByteDance",
                StartDate = new DateOnly(2024, 07, 15),
            },
        ];

        List<TaskX> tasks =
        [
            new()
            {
                Name = "Xác định ý tưởng",
                Status = 0,
                Deadline = new DateTime(2024, 10, 21, 12, 00, 00),
                ProjectId = 111,
            },
            new()
            {
                Name = "Lập kế hoạch",
                Status = 0,
                Deadline = new DateTime(2024, 10, 21, 12, 00, 00),
                ProjectId = 1,
            },
            new()
            {
                Name = "Xây dựng nhóm",
                Status = 0,
                Deadline = new DateTime(2025, 01, 01, 12, 00, 00),
                ProjectId = 1,
            },
        ];

        bool status = await _mainUnitOfWork.SaveChangesWithTransactionAsync(async () =>
        {
            await _mainUnitOfWork.Project.Create(projects , cancellationToken);
            await Task.Delay(5000, cancellationToken);
            await _mainUnitOfWork.Task.Create(tasks, cancellationToken);
        }, cancellationToken);

        return _helper.Respond(message: status.ToString());
    }

    public async Task<dynamic> UploadFile([FromForm] TestRequest.UploadFile fileDto)
    {
        //foreach (var file in fileDto.Files)
        //{
        //    await _helper.UploadFile(file, "multi");
        //}
        //await _helper.UploadFile(fileDto.File, "single");
        //ValidationResult? validationResult = new ImageFileAttribute(width: 1000, height: 100)
        //    .GetValidationResult(fileDto.File, new ValidationContext(fileDto.File));

        UploadResult uploadResult = await _helper.UploadFileWithFTP(fileDto.File, "single");
        return uploadResult.IsSusscess
            ? _helper.Respond(message: "Susscess to upload file and files!")
            : _helper.Respond(message: "Failed to upload file and files!");
    }

    public async Task<dynamic> UploadChunk([FromForm] IFormFile chunk, [FromForm] int chunkNumber, [FromForm] string fileName)
    {
        var result = await _helper.UploadChunk(chunk, chunkNumber, fileName);
        return _helper.Respond(message: result.ToString());
    }

    public async Task<dynamic> MergeChunks([FromForm] string fileName, [FromForm] int totalChunks)
    {
        var result = await _helper.MergeChunks(fileName, totalChunks);
        return _helper.Respond(message: result.ToString());
    }
}
