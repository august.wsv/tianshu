﻿using Application.Requests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Application.Services.Contracts;

public interface IExampleService
{
    Task<dynamic> Query(string? username, int page = 1, int pageSize = 10);
    Task<dynamic> QueryII();
    Task<dynamic> Command(CancellationToken cancellationToken);
    Task<dynamic> UploadFile([FromForm] TestRequest.UploadFile fileDto);
    Task<dynamic> UploadChunk([FromForm] IFormFile chunk, [FromForm] int chunkNumber, [FromForm] string fileName);
    Task<dynamic> MergeChunks([FromForm] string fileName, [FromForm] int totalChunks);
}
