﻿using Application.Requests;

namespace Application.Services.Contracts;

public interface ICommonService
{
    dynamic GetJwtConfig();
    dynamic ConfirmForgottenPassword(UserRequest.ConfirmForgottenPassword userReqConfirmForgottenPassword);
}
