﻿using Microsoft.AspNetCore.Http;

namespace Application.Services.Contracts;

public interface IAuthService
{
    Task OnConnected(HttpContext context);
    Task OnDisconnected(HttpContext context);
    Task OnCompleteRegister(HttpContext context);
}
