﻿using Application.Requests;

namespace Application.Services.Contracts;

public interface IUserService
{
    Task<dynamic> Login(UserRequest.Login userReqLogin);
    Task<dynamic> GetToken(UserRequest.GetToken userReqGetToken);
    Task<dynamic> Logout(UserRequest.Logout userReqLogout);
    Task<dynamic> PrepareRegister(UserRequest.PrepareRegister userReqPrepareRegister);
    Task<dynamic> CompleteRegister(UserRequest.CompleteRegister userReqCompleteRegister);
    Task<dynamic> RemoveAccount(UserRequest.RemoveAccount userReqRemoveAccount);
    Task<dynamic> FindYourAccount(UserRequest.FindYourAccount userReqFindYourAccount);
    Task<dynamic> CompletePasswordChange(UserRequest.CompletePasswordChange userReqCompletePasswordChange);
}
