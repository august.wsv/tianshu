﻿namespace Application.UtilityClasses;

public class UserLoginCountCacheValue
{
    public int Count { get; set; }
    public DateTimeOffset ExpiredTo { get; set; }
}
