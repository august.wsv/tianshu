﻿namespace Application.UtilityClasses;

public class StaticValue
{
    public static readonly List<SessonLive> sessionLives = [];

    public static readonly List<CompleteRegisterLive> completeRegisterLives = [];
}