﻿namespace Application.UtilityClasses;

public class SessonLive
{
    public List<string> ConnIds { get; set; } = [];
    public string SessonId { get; set; }
    public string IpAddress { get; set; }
    public string BrowserType { get; set; }
}
