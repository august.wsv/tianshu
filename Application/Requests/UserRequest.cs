﻿using System.ComponentModel.DataAnnotations;

namespace Application.Requests;

public class UserRequest
{
    public class Login
    {
        [Required(ErrorMessage = "Username can't null!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password can't null!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "NextOtpCode can't null!")]
        public string NextOtpCode { get; set; }
    }

    public class GetToken
    {
        [Required(ErrorMessage = "Session Id can't null!")]
        public string SessionId { get; set; }
    }

    public class Logout
    {
        [Required(ErrorMessage = "Session Id can't null!")]
        public string SessionId { get; set; }
    }

    public class PrepareRegister
    {
        [Required(ErrorMessage = "Full name can't null!")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Username can't null!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password can't null!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Comfirm Password can't null!")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ComfirmPassword { get; set; }
    }

    public class CompleteRegister
    {
        [Required(ErrorMessage = "NextOtpCode can't null!")]
        public string NextOtpCode { get; set; }

        [Required(ErrorMessage = "UserCacheKey can't null!")]
        public string UserCacheKey { get; set; }

        [Required(ErrorMessage = "WillLogin can't null!")]
        public bool WillLogin { get; set; }
    }

    public class RemoveAccount
    {
        [Required(ErrorMessage = "Password can't null!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "NextOtpCode can't null!")]
        public string NextOtpCode { get; set; }
    }

    public class FindYourAccount
    {
        [Required(ErrorMessage = "Username can't null!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "RegisterDate can't null!")]
        public DateOnly? RegisterDate { get; set; }
    }

    public class ConfirmForgottenPassword
    {
        [Required(ErrorMessage = "ConfirmForgottenPasswordCacheKey can't null!")]
        public string FindYourAccountCacheKey { get; set; }

        [Required(ErrorMessage = "NextOtpCode can't null!")]
        public string NextOtpCode { get; set; }
    }

    public class CompletePasswordChange
    {
        [Required(ErrorMessage = "Username can't null!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password can't null!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Comfirm Password can't null!")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ComfirmPassword { get; set; }
    }
}
