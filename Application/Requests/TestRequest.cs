﻿using Kernel.Attributes;
using Kernel.Constants;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.Requests;

public class TestRequest
{
    public class UploadFile
    {
        [Required(ErrorMessage = "File must not be null!")]
        [MaxFileSize((3 * (long)FileSize.GB), ErrorMessage = "The file size should not exceed 3 GB.")]
        public IFormFile? File { get; set; }

        [ImageFile(scale: 9D / 16)]
        [MaxFileSize(1 * 1024 * 1024, ErrorMessage = "The file size should not exceed 1 MB.")]
        public List<IFormFile>? Files { get; set; }
    }
}
