﻿using Kernel.Base.Contracts.IModelBase;
using System.Text.Json.Serialization;

namespace Application.Responses;

public class ProjectResponse : IModelJustIdIsNumber
{
    [JsonIgnore]
    public long Id { get; set; }

    public string? Name { get; set; }

    public ICollection<WTaskX> Tasks { get; set; } = [];

    public class WTaskX
    {
        public long Id { get; set; }

        public string? Name { get; set; }

        public DateTime Deadline { get; set; }
    }
}
