﻿using Kernel.Base.Contracts.IModelBase;
using System.Text.Json.Serialization;

namespace Application.Responses;

public class TaskResponse : IModelJustIdIsNumber
{
    [JsonIgnore]
    public long Id { get; set; }

    public string? Name { get; set; }

    public DateTime Deadline { get; set; }

    public WProject? Project { get; set; }

    public class WProject
    {
        public long Id { get; set; }

        public string? Name { get; set; }
    }
}
