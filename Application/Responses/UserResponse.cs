﻿namespace Application.Responses;

using Kernel.Base.Contracts.IModelBase;
using System.Text.Json.Serialization;

public class UserResponse
{
    public class Login
    {
        [JsonIgnore]
        public long Id { get; set; }

        [JsonIgnore]
        public string FullName { get; set; }

        public string Username { get; set; }

        public string EncryptedPassword { get; set; }

        public string SecretKeyFor2FA { get; set; }
    }
        
    public class PrepareRegister
    {
        public string Username { get; set; }
    }

    public class ShowPublic {
        public string FullName { get; set; }

        public string Username { get; set; }
    }

    public class ShowPublicII : IModelJustIdIsNumber
    {
        public long Id { get; set; }

        public string FullName { get; set; }

        public string Username { get; set; }
    }

    public class ForCache : IModelJustDeletedAt
    {
        public string FullName { get; set; }

        public string Username { get; set; }

        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }
    }

    public class FindYourAccount
    {
        public long Id { get; set; }

        public string FullName { get; set; }

        public string Username { get; set; }

        public DateTime CreatedAt { get; set; }

        public string SecretKeyFor2FA { get; set; }
    }

    public class ConfirmForgottenPassword
    {
        public string Username { get; set; }

        public string SecretKeyFor2FA { get; set; }
    }
}
