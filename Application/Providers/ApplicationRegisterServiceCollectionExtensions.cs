﻿using Application.Profiles;
using Application.Services.Contracts;
using Application.Services.Definitions;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Providers;

public static class ApplicationRegisterServiceCollectionExtensions
{
    public static IServiceCollection AddApplicationRegister(this IServiceCollection services)
    {
        //@Register Services
        services
            .AddScoped<IExampleService, ExampleService>()
            .AddScoped<IUserService, UserService>()
            .AddScoped<IAuthService, AuthService>()
            .AddScoped<ICommonService, CommonService>();
        //@Register Profiles
        services
            //.AddAutoMapper(typeof(ExampleMapperProfile).Assembly)
            .AddAutoMapper(typeof(AutoMapperProfile).Assembly);
        return services;
    }
}
