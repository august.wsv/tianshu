﻿namespace Application.Constants;

public enum PersonType
{
    User = 1,
    Admin = 2
}
