﻿using Application.Requests;
using Application.Responses;
using AutoMapper;
using Domain.Models;

namespace Application.Profiles;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<UserRequest.PrepareRegister, User>();

        CreateMap<User, UserResponse>();
    }
}
