﻿using Kernel.Base.Definitions.ModelBase;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Domain.Models;

public class TaskX : ModelBaseIdIsNumber
{
    [Required]
    public string Name { get; set; }

    [Required]
    public int Status { get; set; }

    [Required]
    public DateTime Deadline { get; set; }

    [Required]
    [JsonIgnore]
    [ForeignKey(nameof(Project.Id))]
    public long ProjectId { get; set; }

    public virtual Project Project { get; set; }
}
