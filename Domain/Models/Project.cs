﻿using Kernel.Base.Definitions.ModelBase;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models;

public class Project : ModelBaseIdIsNumber
{
    [Required]
    public string? Name { get; set; }

    [Required]
    public string? Customer { get; set; }

    [Required]
    public DateOnly? StartDate { get; set; }

    public DateOnly? EndDate { get; set; }

    public virtual ICollection<TaskX> Tasks { get; } = [];
}
