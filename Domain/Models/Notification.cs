﻿using Kernel.Base.Definitions.ModelBase;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models;

public class Notification : ModelBaseIdIsLetter
{
    [Required]
    public int PersonType { get; set; }

    [Required]
    public long PersonId { get; set; }

    [Required]
    public int Type { get; set; }

    [Required]
    [JsonIgnore]
    public string Description { get; set; }
    
    [NotMapped]
    public Dictionary<string, object> KeyValuePairs
    {
        get
        {
            if (string.IsNullOrEmpty(Description))
            {
                return new Dictionary<string, object>();
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(Description);
        }
        set
        {
            Description = Newtonsoft.Json.JsonConvert.SerializeObject(value);
        }
    }

    [Required]
    public bool IsRead { get; set; } = false;
}
