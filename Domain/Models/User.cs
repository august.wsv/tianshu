﻿using Kernel.Base.Definitions.ModelBase;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models;

[Index(nameof(Username), IsUnique = true)]
public class User : ModelBaseIdIsNumber
{
    [Required]
    public string FullName { get; set; }

    [Required]
    public string Username { get; set; }

    [Required]
    public string EncryptedPassword { get; set; }

    [Required]
    public string SecretKeyFor2FA { get; set; }
}
