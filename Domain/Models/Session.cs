﻿using Kernel.Base.Definitions.ModelBase;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models;

public class Session : ModelBaseIdIsLetter
{
    [Required]
    public string TokenHash { get; set; }

    [Required]
    public int PersonType { get; set; }

    [Required]
    public long PersonId { get; set; }

    [Required]
    public long ExpiredTo { get; set; }

    public string? IpAddress { get; set; }

    public string? BrowserType { get; set; }
}
