﻿using System.Security.Cryptography;

namespace Otp;
public class InMemoryKey : IKeyProvider
{
    private readonly object _stateSync = new();

    private readonly byte[] _keyData;

    private readonly int _keyLength;

    public InMemoryKey(byte[] key)
    {
        if (key == null || key.Length == 0)
        {
            throw new ArgumentException("The key must not be empty");
        }

        _keyLength = key.Length;
        int num = (int)Math.Ceiling(key.Length / 16m) * 16;
        _keyData = new byte[num];
        Array.Copy(key, _keyData, key.Length);
    }

    private byte[] GetCopyOfKey()
    {
        byte[] array = new byte[_keyLength];
        lock (_stateSync)
        {
            Array.Copy(_keyData, array, _keyLength);
            return array;
        }
    }
    private static HMAC CreateHmacHash(HashMode otpHashMode)
    {
        return otpHashMode switch
        {
            HashMode.Sha256 => new HMACSHA256(),
            HashMode.Sha512 => new HMACSHA512(),
            _ => new HMACSHA1(),
        };
    }

    public byte[] ComputeHmac(HashMode mode, byte[] data)
    {
        using HMAC hMAC = CreateHmacHash(mode);
        byte[] copyOfKey = GetCopyOfKey();
        try
        {
            hMAC.Key = copyOfKey;
            return hMAC.ComputeHash(data);
        }
        finally
        {
            KeyUtilities.Destroy(copyOfKey);
        }
    }
}