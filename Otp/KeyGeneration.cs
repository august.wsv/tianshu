﻿using System.Security.Cryptography;

namespace Otp;
public static class KeyGeneration
{
    public static byte[] GenerateRandomKey(int length)
    {
        byte[] array = new byte[length];
        using RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();
        randomNumberGenerator.GetBytes(array);
        return array;
    }
}