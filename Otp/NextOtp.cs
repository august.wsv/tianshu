﻿using System.Numerics;
using System.Text;

namespace Otp;
public class NextOtp
{
    private readonly IKeyProvider _secretKey;

    private readonly HashMode _hashMode;

    private readonly int _step;

    private readonly int _totpSize;

    private static DateTime CorrectedUtcNow => DateTime.UtcNow;

    private const string _allowedCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public NextOtp(byte[] secretKey, int step = 30, HashMode mode = HashMode.Sha1, int totpSize = 6)
    {
        if (secretKey == null || secretKey.Length == 0)
        {
            throw new ArgumentException("secretKey empty");
        }
        _secretKey = new InMemoryKey(secretKey);
        _hashMode = mode;
        _step = step;
        _totpSize = totpSize;
    }

    private static string Digits(BigInteger input, int digitCount)
    {
        StringBuilder result = new(digitCount);
        for (int i = 0; i < digitCount + 1; i++)
        {
            if (i >= 1) result.Insert(0, _allowedCharacters[(int)(input % _allowedCharacters.Length)]);
            input = (input / _allowedCharacters.Length) * (_allowedCharacters.Length + 1);
        }
        return result.ToString();
    }

    private static byte[] GetBigEndianBytes(long input)
    {
        byte[] bytes = BitConverter.GetBytes(input);
        Array.Reverse(bytes);
        return bytes;
    }

    public string ComputeOtp()
    {
        long counter = CorrectedUtcNow.Ticks / 10000000 / _step;
        byte[] bigEndianBytes = GetBigEndianBytes(counter);
        byte[] array = _secretKey.ComputeHmac(_hashMode, bigEndianBytes);
        BigInteger calculateOtp =
            (1024UL + array[0]) *
            (2048UL + array[1]) *
            (3072UL + array[2]);
        return Digits(calculateOtp, _totpSize);
    }
}