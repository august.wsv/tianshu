﻿namespace Otp;

public class KeyUtilities
{
    public static void Destroy(byte[] sensitiveData)
    {
        ArgumentNullException.ThrowIfNull(sensitiveData);

        new Random().NextBytes(sensitiveData);
    }
}