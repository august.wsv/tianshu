﻿namespace Otp;

public interface IKeyProvider
{
    public byte[] ComputeHmac(HashMode mode, byte[] data);
}