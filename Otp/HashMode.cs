﻿namespace Otp;

public enum HashMode
{
    Sha1,
    Sha256,
    Sha512
}