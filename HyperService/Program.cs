﻿using Application.Profiles;
using HyperService.Providers;
using HyperService.Rest.Middlewares;
using Infrastructure.Data;
using Kernel.Constants;
using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Text;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
IServiceCollection serviceCollection = builder.Services;
ConfigurationManager configurationManager = builder.Configuration;
string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

#region builder
//---CUSTOM---
//@Add Database Context
serviceCollection.AddDbContext<DataContext>(option => option.UseSqlServer(
    connectionString: configurationManager.GetConnectionString("DefaultConnectString"), 
    sqlServerOptionsAction: option => option.MigrationsAssembly("HyperService")
));
//@Add DI
serviceCollection.AddPorciDIAndPolicy();
//@Add Limit Upload file size - 4GB (IIS 10)
serviceCollection.Configure<FormOptions>(option => option.MultipartBodyLengthLimit = 4 * (long)FileSize.GB);
//@Add Authentication JwtBearer
var privateKey = File.ReadAllText(configurationManager["JWTSecretKeyFile:PrivateKey"] ?? "");
var rsa = RSA.Create();
rsa.ImportFromPem(privateKey.ToCharArray());
var signingPrivateKey = new RsaSecurityKey(rsa);
serviceCollection
    .AddAuthentication()
    .AddJwtBearer(options => {
        options.SaveToken = true;
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = signingPrivateKey,
            ValidateIssuer = false,
            ValidateAudience = false,
            RequireSignedTokens = true,
            ClockSkew = TimeSpan.Zero
        };
    }
);

//serviceCollection.Configure<KestrelServerOptions>(options =>
//{
//    options.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(5);
//    options.Limits.Http2.KeepAlivePingDelay = TimeSpan.FromSeconds(30);
//});

//@Add Cors
serviceCollection.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins, policy =>
    {
        policy.WithOrigins("https://localhost:7210" ?? "").AllowAnyHeader().AllowAnyMethod();
        policy.WithOrigins("https://localhost:7019" ?? "").AllowAnyHeader().AllowAnyMethod();
    });
});
//@Add HttpContextAccessor
serviceCollection.AddHttpContextAccessor();
//@Add AutoMapper
serviceCollection.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
serviceCollection.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
//---DEFAULT---
serviceCollection.AddControllers();
serviceCollection.AddMemoryCache();

#endregion

WebApplication webApplication = builder.Build();

webApplication.UseRouting();
//Use status code pages
webApplication.UseStatusCodePages(async context =>
    await context.HttpContext.RequestServices.
        GetRequiredService<IStatusCode>().
        HandleUnauthorized(context.HttpContext)
);
//Use Cors
webApplication.UseCors(MyAllowSpecificOrigins);
//Use StaticFiles
webApplication.UseStaticFiles();
//Use Authorization
webApplication.UseAuthorization();
webApplication.UseHttpsRedirection();
//Use Middleware
webApplication
    .UseMiddleware<AuthMiddleware>();
//Use WebSockets

WebSocketOptions webSocketOptions = new()
{
    KeepAliveInterval = TimeSpan.FromMinutes(2)
};
webSocketOptions.AllowedOrigins.Add("https://localhost:7210");
webSocketOptions.AllowedOrigins.Add("https://localhost:7019");
webApplication.UseWebSockets(webSocketOptions);
//@Create table and add data
using (IServiceScope scope = webApplication.Services.CreateScope())
{
    IServiceProvider services = scope.ServiceProvider;
    DataContext context = services.GetRequiredService<DataContext>();
    DataInitializer.Seed(context);
}

webApplication.MapControllers();

webApplication.Run();