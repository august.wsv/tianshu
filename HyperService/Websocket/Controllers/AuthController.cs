﻿using Application.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace HyperService.Websocket.Controllers;

[Route("api/[controller]/[action]")]
public class AuthController : ControllerBase
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpGet]
    public async Task Comfirm()
    {
        if (HttpContext.WebSockets.IsWebSocketRequest)
        {
            await _authService.OnConnected(HttpContext);            
            await _authService.OnDisconnected(HttpContext);
        }
        else
        {
            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        }
    }

    [HttpGet]
    public async Task CompleteRegister()
    {
        if (HttpContext.WebSockets.IsWebSocketRequest)
        {
            await _authService.OnCompleteRegister(HttpContext);
        }
        else
        {
            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        }
    }
}

