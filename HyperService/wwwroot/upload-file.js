﻿const uploadFileInChunks = async (file) => {
    const chunkSize = 1024 * 1024 * 50; // 50MB
    const totalChunks = Math.ceil(file.size / chunkSize);
    const fileName = file.name;
    const startTime = performance.now(); // Bắt đầu tính thời gian
    const maxConcurrentUploads = 5; // Giới hạn số lượng request đồng thời
    const queue = [];
    let successfulUploads = true;

    const uploadChunk = (chunk, chunkNumber) => {
        const formData = new FormData();
        formData.append("chunk", chunk, file.type);
        formData.append("chunkNumber", chunkNumber);
        formData.append("fileName", fileName);

        return fetch('https://localhost:888/api/Episode/UploadChunk', {
            method: 'POST',
            body: formData
        }).then(response => {
            if (response.ok) {
                return response.json();
            }
        }).then(data => {
            console.log(data.message)
        }).catch(error => {
            successfulUploads = false;
            console.log('Error uploading chunk:')
        });
    };

    for (let chunkNumber = 0; chunkNumber < totalChunks; chunkNumber++) {
        if (!successfulUploads) {
            console.log('Upload process stopped due to previous errors.');
            break;
        }
        const start = chunkNumber * chunkSize;
        const end = Math.min(start + chunkSize, file.size);
        const chunk = file.slice(start, end);

        queue.push(uploadChunk(chunk, chunkNumber));

        if (queue.length >= maxConcurrentUploads) {
            await Promise.allSettled(queue);
            queue.length = 0; // Clear the queue
        }
    }

    // Wait for remaining uploads to finish
    await Promise.allSettled(queue);

    if (successfulUploads) {
        const mergeData = new FormData();
        mergeData.append("fileName", fileName);
        mergeData.append("totalChunks", totalChunks);

        await fetch('https://localhost:888/api/Episode/MergeChunks', {
            method: 'POST',
            body: mergeData
        }).then(response => response.json())
            .then(data => {
                console.log(data.message);
                const endTime = performance.now(); // Kết thúc tính thời gian
                const totalTime = endTime - startTime; // Tính tổng thời gian
                console.log(`Total time (milliseconds): ${totalTime}`)
            })
            .catch(error => console.error('Error merging chunks:', error));
    } else {
        console.error('Not all chunks were successfully uploaded');
    }
};

// Usage example
document.getElementById('fileInput').addEventListener('change', (event) => {
    const file = event.target.files[0];
    if (file) {
        uploadFileInChunks(file);
    }
});