﻿var connectionEstablished = false;

function jsonToQuery(jsonObj) {
    return Object
        .keys(jsonObj)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(jsonObj[key])}`)
        .join('&');
};

const queryString = jsonToQuery({
    session_id: localStorage.getItem('session_id'),
});

var connectionUrl = `wss://localhost:999/api/Auth/Comfirm?${queryString}`;

function Connect() {
    var socket = new WebSocket(connectionUrl);

    socket.onopen = function (event) {
        connectionEstablished = true;
    };

    socket.onclose = function (event) {
        connectionEstablished = false;
    };

    socket.onerror = function (event) { };

    socket.onmessage = function (event) { };
};

Connect();

var settings = {
    "url": "https://localhost:999/api/Something/Query?username=aab&pageSize=2000",
    "method": "GET",
    "timeout": 0,
    "headers": {
        "Authorization": `Bearer ${localStorage.getItem('access_token')}`
    },
};

let retryCount = 0;
const maxRetries = 10;

function checkConnection() {
    if (retryCount >= maxRetries) {
        console.log('Reached maximum retry attempts.');
        return;
    }

    if (connectionEstablished) {
        $.ajax(settings).done(function (response) {
            console.log(response);
        });
    } else {
        retryCount++;
        setTimeout(checkConnection, 100);
    }
}

checkConnection();