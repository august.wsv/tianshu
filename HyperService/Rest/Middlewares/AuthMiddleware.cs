﻿using Application.Services.Contracts;
using Domain.Models;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Base.Definitions;
using Kernel.Support.Contracts;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Cryptography;

namespace HyperService.Rest.Middlewares;

public class AuthMiddleware : MiddlewareBase
{
    private readonly RequestDelegate _next;

    private readonly IServiceProvider _scopeFactory;

    private readonly RsaSecurityKey _signingPublicKey;

    private readonly IHelper _helper;

    private readonly IConfiguration _configuration;

    public override List<string> ApplyList => [
        "api#Test@Test1[{id}]",
        "api#Example@Query[]",
        "api#User@RemoveAccount[]"
    ];

    public AuthMiddleware
    (
        RequestDelegate next,
        IServiceProvider scopeFactory,
        IHelper helpers,
        IConfiguration configuration
    )
    {
        _next = next;
        _scopeFactory = scopeFactory;
        _helper = helpers;
        _configuration = configuration;

        var publicKey = File.ReadAllText(_configuration["JWTSecretKeyFile:PublicKey"] ?? "");
        var rsa = RSA.Create();
        rsa.ImportFromPem(publicKey.ToCharArray());
        _signingPublicKey = new RsaSecurityKey(rsa);
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (ApplyList.Contains(GetPath(context)))
        {
            using IServiceScope scope = _scopeFactory.CreateScope();
            IMainUnitOfWork mainUnitOfWork = scope.ServiceProvider.GetRequiredService<IMainUnitOfWork>();
            IAuthService authService = scope.ServiceProvider.GetRequiredService<IAuthService>();
            string token = context.Request.Headers.Authorization.ToString()["Bearer ".Length..];
            await VerifyToken(token, context);
            Session? session = await mainUnitOfWork.Session.GetObjectByField<Session>(session => 
                _helper.ComputeHashJwt(token).Equals(session.TokenHash)
            );
            await SessionValidation(context, session);
        }
        await _next(context);
    }

    private async Task VerifyToken (string token, HttpContext context)
    {
        try
        {
            var handler = new JwtSecurityTokenHandler();
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingPublicKey,
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireSignedTokens = true,
                ClockSkew = TimeSpan.Zero
            };

            SecurityToken validatedToken;
            var principal = handler.ValidateToken(token, validationParameters, out validatedToken);

            context.User = principal;
        }
        catch (SecurityTokenValidationException)
        {
            // Xử lý lỗi xác thực token
            await _helper.ContextRespond(context, (int)HttpStatusCode.Unauthorized, $"{(int)HttpStatusCode.Unauthorized}");
            return;
        }
        catch (Exception ex)
        {
            // Xử lý các lỗi khác
            await _helper.ContextRespond(context, (int)HttpStatusCode.InternalServerError, $"{(int)HttpStatusCode.Unauthorized}");
            return;
        }
    }

    private async Task SessionValidation(HttpContext context, Session? session)
    {
        if (session != null)
        {
            if (session.IpAddress == null || session.BrowserType == null)
            {
                await _helper.ContextRespond(context, 403, "Websocket connection has not been established");
                return;
            }
            else if
            (
                session.IpAddress != null && session.BrowserType != null &&
                (
                    !session.IpAddress.Equals(context.Connection.RemoteIpAddress?.ToString()) ||
                    !session.BrowserType.Equals(context.Request.Headers.UserAgent.ToString())
                )
            )
            {
                await _helper.ContextRespond(context, 403, "Different IP addresses, different applications");
                return;
            }
        }
        else if (session is null)
        {
            await _helper.ContextRespond(context, 403, "Not found this session");
            return;
        }
        Console.WriteLine("AuthMiddleware: Pass");
    }
}
