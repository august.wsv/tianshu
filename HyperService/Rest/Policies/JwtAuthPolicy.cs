﻿using Application.Constants;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace HyperService.Rest.Policies;

public class JwtAuthPolicy
{
    public static void User(AuthorizationPolicyBuilder policy)
    {
        policy.RequireAssertion(context =>
        {
            Claim? personTypeClaim = context.User.FindFirst("person-type");

            if (personTypeClaim is not null)
            {
                int personType = int.Parse(personTypeClaim.Value);
                return (PersonType)personType == PersonType.User;
            }

            return false;
        });
    }

    public static void Admin(AuthorizationPolicyBuilder policy)
    {
        policy.RequireAssertion(context =>
        {
            Claim? personTypeClaim = context.User.FindFirst("person-type");

            if (personTypeClaim is not null)
            {
                int personType = int.Parse(personTypeClaim.Value);
                return (PersonType)personType == PersonType.Admin;
            }

            return false;
        });
    }
}