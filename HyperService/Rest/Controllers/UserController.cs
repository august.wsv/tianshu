﻿using Application.Requests;
using Application.Services.Contracts;
using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HyperService.Rest.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IHelper _helper;

    public UserController(IUserService userService, IHelper helpers)
    {
        _userService = userService;
        _helper = helpers;
    }

    [HttpPost]
    public async Task<IActionResult> Login([FromForm] UserRequest.Login userReqLogin)
    {
        if (ModelState.IsValid)
        { return await _userService.Login(userReqLogin); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> GetToken([FromForm] UserRequest.GetToken userReqGetToken)
    {
        if (ModelState.IsValid)
        { return await _userService.GetToken(userReqGetToken); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> Logout([FromForm] UserRequest.Logout userReqLogout)
    {
        if (ModelState.IsValid)
        { return await _userService.Logout(userReqLogout); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> PrepareRegister([FromForm] UserRequest.PrepareRegister userReqPrepareRegister)
    {
        if (ModelState.IsValid)
        { return await _userService.PrepareRegister(userReqPrepareRegister); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> CompleteRegister([FromForm] UserRequest.CompleteRegister userReqCompleteRegister)
    {
        if (ModelState.IsValid)
        { return await _userService.CompleteRegister(userReqCompleteRegister); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    [Authorize(Policy = "user")]
    public async Task<IActionResult> RemoveAccount([FromForm] UserRequest.RemoveAccount userReqRemoveAccount)
    {
        if (ModelState.IsValid)
        { return await _userService.RemoveAccount(userReqRemoveAccount); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> FindYourAccount([FromForm] UserRequest.FindYourAccount userReqFindYourAccount)
    {
        if (ModelState.IsValid)
        { return await _userService.FindYourAccount(userReqFindYourAccount); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> CompletePasswordChange([FromForm] UserRequest.CompletePasswordChange userReqCompletePasswordChange)
    {
        if (ModelState.IsValid)
        { return await _userService.CompletePasswordChange(userReqCompletePasswordChange); }
        return _helper.GetErrorResponse(ModelState);
    }
}
