﻿using Application.Requests;
using Application.Services.Contracts;
using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace HyperService.Rest.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class CommonController : ControllerBase
{
    private readonly ICommonService _commonService;

    private readonly IHelper _helper;

    public CommonController(ICommonService systemService, IHelper helper)
    {
        _commonService = systemService;
        _helper = helper;
    }

    [HttpPost]  
    public IActionResult GetJwtConfig()
    {
        if (ModelState.IsValid)
        { return _commonService.GetJwtConfig(); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public IActionResult ConfirmForgottenPassword([FromForm] UserRequest.ConfirmForgottenPassword userReqConfirmForgottenPassword) 
    {
        if (ModelState.IsValid)
        { return _commonService.ConfirmForgottenPassword(userReqConfirmForgottenPassword); }
        return _helper.GetErrorResponse(ModelState);
    }
}
