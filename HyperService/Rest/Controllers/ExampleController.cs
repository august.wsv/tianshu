﻿using Application.Requests;
using Application.Services.Contracts;
using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HyperService.Rest.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class ExampleController : ControllerBase
{
    private readonly IExampleService _exampleService;

    private readonly IHelper _helper;

    public ExampleController(IExampleService exampleService, IHelper helper)
    {
        _exampleService = exampleService;
        _helper = helper;
    }

    [HttpGet]
    [Authorize]
    public async Task<IActionResult> Query(string? username, int page = 1, int pageSize = 10)
    {
        if (ModelState.IsValid)
        { return await _exampleService.Query(username, page, pageSize); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpGet]
    public async Task<IActionResult> QueryII()
    {
        if (ModelState.IsValid)
        { return await _exampleService.QueryII(); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> Command(CancellationToken cancellationToken)
    {
        if (ModelState.IsValid)
        { return await _exampleService.Command(cancellationToken); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    [DisableRequestSizeLimit]
    public async Task<IActionResult> UploadFile([FromForm] TestRequest.UploadFile fileDto)
    {
        if (ModelState.IsValid)
        { return await _exampleService.UploadFile(fileDto); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    [DisableRequestSizeLimit]
    public async Task<IActionResult> UploadChunk([FromForm] IFormFile chunk, [FromForm] int chunkNumber, [FromForm] string fileName)
    {
        if (ModelState.IsValid)
        { return await _exampleService.UploadChunk(chunk, chunkNumber,fileName); }
        return _helper.GetErrorResponse(ModelState);
    }

    [HttpPost]
    public async Task<IActionResult> MergeChunks([FromForm] string fileName, [FromForm] int totalChunks)
    {
        if (ModelState.IsValid)
        { return await _exampleService.MergeChunks(fileName, totalChunks); }
        return _helper.GetErrorResponse(ModelState);
    }
}
