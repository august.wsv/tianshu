﻿using Application.Providers;
using HyperService.Rest.Policies;
using Infrastructure.Providers;
using Kernel.Providers;

namespace HyperService.Providers;

public static class PorciDIAndPolicyServicesCollectionExtensions
{
    public static IServiceCollection AddPorciDIAndPolicy(this IServiceCollection services)
    {
        //@Register ...
        services.AddApplicationRegister();
        services.AddInfrastructureRegister();
        services.AddKernelRegister();

        //@Register Policy
        services.AddAuthorizationBuilder()
            .AddPolicy("user", JwtAuthPolicy.User)
            .AddPolicy("admin", JwtAuthPolicy.Admin);
        return services;
    }
}
