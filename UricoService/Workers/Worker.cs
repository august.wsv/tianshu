﻿using Domain.Models;
using Grpc.Net.Client;
using Infrastructure.Repositorys.Contracts;
using Infrastructure.UnitOfWorks.Contracts;
using Kernel.Base.Contracts;
using Kernel.Constants;
using MailerService;
using Microsoft.Extensions.Logging;

namespace UricoService.Workers;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;

    private readonly IServiceProvider _serviceProvider;

    public Worker(ILogger<Worker> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using IServiceScope scope = _serviceProvider.CreateScope();
        IMainUnitOfWork mainUnitOfWork = scope.ServiceProvider.GetRequiredService<IMainUnitOfWork>();
        List<User> users = await mainUnitOfWork.User.GetList<User>();
        using GrpcChannel channel = GrpcChannel.ForAddress("https://localhost:447");
        var client = new SendMail.SendMailClient(channel);
        while (!stoppingToken.IsCancellationRequested)
        {
            var request = new ToClientRequest
            {
                Email = "nhatvu.xws@gmail.com",
                Code = "DSAFAEF",
                FilePath = "E:/Users.txt"
            };

            try
            {
                var response = await client.ToClientAsync(request);
                _logger.LogInformation("Email sent status: {status}", response.Status);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error calling gRPC service");
            }

            await Task.Delay(1000, stoppingToken);
        }
    }
}
