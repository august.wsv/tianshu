﻿using Infrastructure.Providers;

namespace UricoService.Providers;

public static class PorciDIServicesCollectionExtensions
{
    public static IServiceCollection AddPorciDI(this IServiceCollection services)
    {
        //@Register ...
        services.AddInfrastructureRegister();
        return services;
    }
}
