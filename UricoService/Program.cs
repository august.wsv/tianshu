using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using UricoService.Providers;
using UricoService.Workers;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
ConfigurationManager configurationManager = builder.Configuration;
IServiceCollection serviceCollection = builder.Services;

serviceCollection.AddDbContext<DataContext>(option => option.UseSqlServer(
    connectionString: configurationManager.GetConnectionString("DefaultConnectString")
));

serviceCollection.AddPorciDI();

serviceCollection.AddMemoryCache();

serviceCollection.AddHostedService<Worker>();

var host = builder.Build();
host.Run();
