﻿using Infrastructure.Providers;

namespace MailerService.Providers;

public static class PorciDIServicesCollectionExtensions
{
    public static IServiceCollection AddPorciDI(this IServiceCollection services)
    {
        //@Register ...
        services.AddInfrastructureRegister();
        return services;
    }
}
