using Infrastructure.Data;
using MailerService.Providers;
using MailerService.Services;
using Microsoft.EntityFrameworkCore;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
ConfigurationManager configurationManager = builder.Configuration;
IServiceCollection serviceCollection = builder.Services;

serviceCollection.AddGrpc();

serviceCollection.AddDbContext<DataContext>(option => option.UseSqlServer(
    connectionString: configurationManager.GetConnectionString("DefaultConnectString")
));

serviceCollection.AddPorciDI();

serviceCollection.AddMemoryCache();

WebApplication webApplication = builder.Build();

webApplication.MapGrpcService<SendMailService>();

webApplication.Run();