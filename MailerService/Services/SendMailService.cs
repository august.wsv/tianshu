using Domain.Models;
using Grpc.Core;
using Infrastructure.Repositorys.Contracts;
using MailerService.Actions;

namespace MailerService.Services;

public class SendMailService : SendMail.SendMailBase
{
    private readonly IUserRepository _userRepository;

    public SendMailService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public override async Task<ToClientResponse> ToClient(ToClientRequest request, ServerCallContext context)
    {
        List<User> users = await _userRepository.GetList<User>();
        bool result = await ToClientAction.EmailConfirmationAsync(request.Email, request.Code, request.FilePath);
        if (result)
        {
            return await Task.FromResult(new ToClientResponse { Status = true });
        }
        else
        {
            return await Task.FromResult(new ToClientResponse { Status = false });
        }
    }
}