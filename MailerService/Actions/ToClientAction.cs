﻿using System.Net.Mail;
using System.Net;
using RazorLight;

namespace MailerService.Actions;

public class ToClientAction
{
    public static async Task<bool> EmailConfirmationAsync(string toAddress, string code, string attachmentFilePath)
    {
        string currentDirectory = Directory.GetCurrentDirectory();
        string templateFolderPath = Path.Combine(currentDirectory, "Templates");
        string templateName = "EmailConfirmation.cshtml";
        RazorLightEngine engine = new RazorLightEngineBuilder()
            .UseFileSystemProject(templateFolderPath)
            .Build();
        string fromAddress = "sws.pxl@gmail.com";
        string password = "zllwxnyqdirrnvvq";
        string subject = "Email confirmation";
        string body = await engine.CompileRenderAsync(templateName, code);

        SmtpClient smtpClient = new("smtp.gmail.com")
        {
            Port = 587,
            Credentials = new NetworkCredential(fromAddress, password),
            EnableSsl = true,
        };

        MailMessage mailMessage = new(fromAddress, toAddress)
        {
            Subject = subject,
            Body = body,
            IsBodyHtml = true,
            From = new MailAddress(fromAddress, "Saler")
        };

        mailMessage.To.Add(new MailAddress(toAddress));

        try
        {
            if (!string.IsNullOrEmpty(attachmentFilePath) && File.Exists(attachmentFilePath))
            {
                Attachment attachment1 = new(attachmentFilePath);
                mailMessage.Attachments.Add(attachment1);
            }
            else
            {
                throw new Exception("Not found file to send email!");
            }
            smtpClient.Send(mailMessage);
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error sending email: " + ex.Message);
            return false;
            
        }
    }
}
