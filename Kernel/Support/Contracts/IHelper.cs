﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using X.PagedList;
using Kernel.UtilityClasses;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Kernel.Support.Contracts;

public interface IHelper
{
    public JsonResult Respond(int status = 200, string message = "Success", object? data = null, object? metadata = null, object? errors = null);
    public Task ContextRespond(HttpContext context, int status, string message);
    public JsonResult GetErrorResponse(ModelStateDictionary modelState);
    public object GetPaginationMetadata(IPagedList<dynamic> objectPaged, int pageNumber, int pageSize);
    public object GetInfiniteScrollMetadata(IPagedList<dynamic> objectPaged, int pageNumber, int pageSize);
    public Task<UploadResult> UploadFile<TFile>(TFile? fileField, string folderName) where TFile : IFormFile;
    public Task<UploadResult> UploadFileWithFTP<TFile>(TFile? fileField, string folderName) where TFile : IFormFile;
    public JwtSecurityToken GetJwtTokenHmacSha256(List<Claim> authClaim, DateTime time);
    public JwtSecurityToken GetJwtTokenRsaSha256(List<Claim> authClaim, DateTime time);
    public Task<bool> UploadChunk(IFormFile chunk, int chunkNumber, string fileName);
    public Task<bool> MergeChunks(string fileName, int totalChunks);
    public string ConvertToBase26(long num);
    public string ComputeHashJwt(string token);
}
