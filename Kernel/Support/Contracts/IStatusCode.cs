﻿using Microsoft.AspNetCore.Http;

namespace Kernel.Support.Contracts;

public interface IStatusCode
{
    Task HandleUnauthorized(HttpContext context);
}
