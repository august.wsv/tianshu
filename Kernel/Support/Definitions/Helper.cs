﻿using FluentFTP;
using Kernel.Support.Contracts;
using Kernel.UtilityClasses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using X.PagedList;
using System.Security.Cryptography;

namespace Kernel.Support.Definitions;

public class Helper : IHelper
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    private readonly IWebHostEnvironment _webHostEnvironment;

    private readonly IConfiguration _configuration;

    public Helper(
        IHttpContextAccessor httpContextAccessor,
        IWebHostEnvironment webHostEnvironment,
        IConfiguration configuration
    )
    {
        _httpContextAccessor = httpContextAccessor;
        _webHostEnvironment = webHostEnvironment;
        _configuration = configuration;
    }
    public JsonResult Respond(
        int status = 200,
        string message = "Success",
        object? data = null,
        object? metadata = null,
        object? errors = null
    )
    {
        dynamic responseObj = new
        {
            Status = status,
            Response = new
            {
                Message = message,
                Data = data,
                Metadata = metadata,
                Errors = errors
            }
        };
        return new JsonResult(responseObj.Response)
        {
            StatusCode = responseObj.Status,
            ContentType = "application/json"
        };
    }

    public async Task ContextRespond(HttpContext context, int status, string message)
    {
        dynamic responseObj = new
        {
            Status = status,
            Response = new
            {
                Message = message,
                Data = null as object,
                Metadata = null as object,
                Errors = null as object
            }
        };
        JsonResult result = new(responseObj.Response)
        {
            StatusCode = responseObj.Status,
            ContentType = "application/json"
        };
        await result.ExecuteResultAsync(new ActionContext
        {
            HttpContext = context
        });
    }

    public JsonResult GetErrorResponse(ModelStateDictionary modelState)
    {
        Dictionary<string, string?> errors = modelState
            .Where(x => x.Value != null && x.Value.Errors.Count > 0)
            .ToDictionary(k => k.Key, v => v.Value?.Errors.Select(e => e.ErrorMessage).First());
        return Respond(status: 422, message: "Error checking data", errors: errors);
    }

    private string GetPageLink(int page, int pageSize)
    {
        HttpContext? httpcontext = _httpContextAccessor.HttpContext;
        if (httpcontext == null)
            return "URL_NOT_AVAILABLE";
        HttpRequest request = httpcontext.Request;
        string baseUrl = $"{request.Scheme}://{request.Host}{request.PathBase}{request.Path}";
        IQueryCollection queryParameters = request.Query;
        Dictionary<string, StringValues> filteredQueryParameters = queryParameters
            .Where(q => q.Key != "page" && q.Key != "pageSize")
            .ToDictionary(q => q.Key, q => q.Value);
        QueryString queryString = new QueryBuilder(filteredQueryParameters).ToQueryString();
        string querySeparator = queryString.Value != "" ? "&" : "?";
        return $"{baseUrl}{queryString}{querySeparator}page={page}&pageSize={pageSize}";
    }

    public object GetPaginationMetadata(IPagedList<dynamic> objectPaged, int pageNumber, int pageSize)
    {
        return new
        {
            page = pageNumber,
            pageCount = objectPaged.PageCount == 0 ? 1 : objectPaged.PageCount,
            prevPageLink = objectPaged.HasPreviousPage ? GetPageLink(pageNumber - 1, pageSize) : null,
            nextPageLink = objectPaged.HasNextPage ? GetPageLink(pageNumber + 1, pageSize) : null,
            firstPageLink = GetPageLink(1, pageSize),
            lastPageLink = GetPageLink(objectPaged.PageCount == 0 ? 1 : objectPaged.PageCount, pageSize)
        };
    }

    public object GetInfiniteScrollMetadata(IPagedList<dynamic> objectPaged, int pageNumber, int pageSize)
    {
        return new
        {
            currentEndIndex = (pageNumber - 1) * pageSize + objectPaged.Count,
            totalRecordCount = objectPaged.TotalItemCount,
            nextPageLink = objectPaged.HasNextPage ? GetPageLink(pageNumber + 1, pageSize) : null
        };
    }

    public async Task<UploadResult> UploadFile<TFile>(TFile? fileField, string folderName) where TFile : IFormFile
    {
        if (fileField == null)
            return new UploadResult { IsSusscess = false };
        string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, folderName);
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
        string fileName = $"{DateTime.UtcNow:yyyyMMddHHmmss}_{fileField.FileName}";
        using (FileStream stream = new(Path.Combine(_webHostEnvironment.WebRootPath, folderName, fileName), FileMode.Create))
        { await fileField.CopyToAsync(stream); }
        return new UploadResult { Path = $"/{folderName}/{fileName}" };
    }

    public async Task<UploadResult> UploadFileWithFTP<TFile>(TFile? fileField, string folderName) where TFile : IFormFile
    {
        if (fileField == null)
            return new UploadResult { IsSusscess = false };
        string fileName = $"{DateTime.UtcNow:yyyyMMddHHmmss}_{fileField.FileName}";
        string ftpFilePath = $"/{folderName}/{fileName}";
        try
        {
            using FtpClient ftpClient = new(
                host: _configuration["FTP:ServerUrl"],
                user: _configuration["FTP:Username"],
                pass: _configuration["FTP:Password"]
            );
            ftpClient.Connect();
            if (!ftpClient.DirectoryExists(folderName))
                ftpClient.CreateDirectory(folderName);
            using Stream stream = fileField.OpenReadStream();
            ftpClient.UploadStream(stream, ftpFilePath);
            ftpClient.Disconnect();
            await Task.CompletedTask;
            return new UploadResult { Path = ftpFilePath };
        }
        catch
        {
            return new UploadResult { IsSusscess = false };
        }
    }

    public JwtSecurityToken GetJwtTokenHmacSha256(List<Claim> authClaim, DateTime time)
    {
        return new JwtSecurityToken(
            issuer: _configuration["JWT:Issuer"],
            audience: _configuration["JWT:Audience"],
            expires: time,
            claims: authClaim,
            signingCredentials: new SigningCredentials(
                key: new SymmetricSecurityKey(key: Encoding.UTF8.GetBytes(_configuration["JWT:Secret"] ?? "")),
                algorithm: SecurityAlgorithms.HmacSha256
            )
        );
    }

    public JwtSecurityToken GetJwtTokenRsaSha256(List<Claim> authClaim, DateTime time)
    {
        string privateKey = File.ReadAllText(_configuration["JWTSecretKeyFile:PrivateKey"] ?? "");
        RSA rsa = RSA.Create();
        rsa.ImportFromPem(privateKey.ToCharArray());
        RsaSecurityKey signingPrivateKey = new(rsa);
        return new JwtSecurityToken(
            issuer: _configuration["JWT:Issuer"],
            audience: _configuration["JWT:Audience"],
            expires: time,
            claims: authClaim,
            signingCredentials: new SigningCredentials(
                key: signingPrivateKey,
                algorithm: SecurityAlgorithms.RsaSha256
            )
        );
    }

    public async Task<bool> UploadChunk(IFormFile chunk, int chunkNumber, string fileName)
    {
        try
        {
            string filePath = Path.Combine(_webHostEnvironment.WebRootPath, fileName + ".part" + chunkNumber);
            using FileStream stream = new FileStream(filePath, FileMode.Create);
            await chunk.CopyToAsync(stream);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public async Task<bool> MergeChunks(string fileName, int totalChunks)
    {
        try
        {
            string finalFilePath = Path.Combine(_webHostEnvironment.WebRootPath, fileName);
            using FileStream finalFileStream = new FileStream(finalFilePath, FileMode.Create);
            for (int i = 0; i < totalChunks; i++)
            {
                string chunkFilePath = Path.Combine(_webHostEnvironment.WebRootPath, fileName + ".part" + i);
                using (FileStream chunkFileStream = new FileStream(chunkFilePath, FileMode.Open))
                {
                    chunkFileStream.CopyTo(finalFileStream);
                }

                File.Delete(chunkFilePath);
            }
            await Task.CompletedTask;
            return true;
        }
        catch
        {
            return false;
        }
    }

    public string ConvertToBase26(long num)
    {
        if (num <= 0)
            throw new ArgumentOutOfRangeException("num", "Input must be positive");

        string result = string.Empty;
        while (num > 0)
        {
            long remainder = (num - 1) % 26;
            result = (char)('A' + remainder) + result;
            num = (num - 1) / 26;
        }

        return result;
    }

    public string ComputeHashJwt(string token)
    {
        using SHA256 sha256 = SHA256.Create();
        byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(token));
        return Convert.ToBase64String(bytes);
    }
}
