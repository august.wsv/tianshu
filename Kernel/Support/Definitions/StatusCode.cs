﻿using Kernel.Support.Contracts;
using Microsoft.AspNetCore.Http;

namespace Kernel.Support.Definitions;

public class StatusCode : IStatusCode
{
    private readonly IHelper _helper;

    public StatusCode(IHelper helpers)
    {
        _helper = helpers;
    }

    public async Task HandleUnauthorized(HttpContext context)
    {
        int statusCode = context.Response.StatusCode;
        string message = statusCode switch
        {
            401 => "Missing or invalid Authorization Bearer token",
            403 => "Deny access to resources",
            404 => "Resource Not Found",
            405 => "Method Not Allowed",
            _ => "Unknown Error",
        };

        await _helper.ContextRespond(context, statusCode, message);
    }
}
