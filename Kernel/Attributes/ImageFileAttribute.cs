﻿using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;

namespace Kernel.Attributes;

public class ImageFileAttribute(int width = 0, int height = 0, int minWidth = 0, int minHeight = 0, int maxWidth = 0, int maxHeight = 0, double scale = 0.0) : ValidationAttribute
{
    private readonly string[] _allowedMimeTypes = ["image/jpeg", "image/png", "image/gif"];

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
            return new ValidationResult("Image file must not be null");

        if (value is IFormFile file)
        {
            return ValidateFile(file);
        }
        else if (value is List<IFormFile> files)
        {
            ConcurrentBag<ValidationResult> validationResults = [];

            Parallel.ForEach(files, file =>
            {
                ValidationResult? result = ValidateFile(file);
                if (result != ValidationResult.Success && result != null)
                    validationResults.Add(result);
            });

            if (!validationResults.IsEmpty)
                return validationResults.FirstOrDefault();
        }

        return ValidationResult.Success;
    }

    private ValidationResult? ValidateFile(IFormFile file)
    {
        if (!_allowedMimeTypes.Contains(file.ContentType))
            return new ValidationResult($"Invalid mime type.");

        using (Stream stream = file.OpenReadStream())
        {
            try
            {
                Image<Rgba32> image = Image.Load<Rgba32>(stream);

                if (width != 0 && image.Width != width)
                    return new ValidationResult($"Image width must be {width}px");

                if (height != 0 && image.Height != height)
                    return new ValidationResult($"Image height must be {height}px");

                if (minWidth != 0 && image.Width < minWidth)
                    return new ValidationResult($"Image width must be at least {minWidth}px");

                if (minHeight != 0 && image.Height < minHeight)
                    return new ValidationResult($"Image height must be at least {minHeight}px");

                if (maxWidth != 0 && image.Width > maxWidth)
                    return new ValidationResult($"Image width must not exceed {maxWidth}px");

                if (maxHeight != 0 && image.Height > maxHeight)
                    return new ValidationResult($"Image height must not exceed {maxHeight}px");

                if (scale != 0.0)
                {
                    double aspectRatio = (double)image.Width / image.Height;

                    if (!(Math.Abs(aspectRatio / scale - 1.0) < 1e-2))
                        return new ValidationResult($"Image dimensions do not match the required scale of {scale}%");
                }
            }
            catch (UnknownImageFormatException)
            {
                return new ValidationResult("Invalid image format");
            }
        }

        return ValidationResult.Success;
    }
}
