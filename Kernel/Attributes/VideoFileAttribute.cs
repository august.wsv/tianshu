﻿using Microsoft.AspNetCore.Http;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using Xabe.FFmpeg;

namespace Kernel.Attributes;

public class VideoFileAttribute(int maxDurationSeconds = 0) : ValidationAttribute
{
    private readonly string[] _allowedMimeTypes = { "video/mp4", "video/quicktime", "video/x-msvideo" };

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
            return new ValidationResult("Video file must not be null");

        if (value is IFormFile file)
        {
            return ValidateFileAsync(file).Result;
        }
        else if (value is List<IFormFile> files)
        {
            ConcurrentBag<ValidationResult> validationResults = new ConcurrentBag<ValidationResult>();

            Parallel.ForEach(files, file =>
            {
                ValidationResult? result = ValidateFileAsync(file).Result;
                if (result != ValidationResult.Success && result != null)
                    validationResults.Add(result);
            });

            if (!validationResults.IsEmpty)
                return validationResults.FirstOrDefault();
        }

        return ValidationResult.Success;
    }

    private async Task<ValidationResult?> ValidateFileAsync(IFormFile file)
    {
        if (!_allowedMimeTypes.Contains(file.ContentType))
            return new ValidationResult($"Invalid mime type.");

        string tempFilePath = Path.GetTempFileName();

        try
        {
            // Copy the file to a temporary location
            using (FileStream stream = new FileStream(tempFilePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            IMediaInfo mediaInfo = await FFmpeg.GetMediaInfo(tempFilePath);
            IVideoStream? videoStream = mediaInfo.VideoStreams.FirstOrDefault();

            if (videoStream == null)
            {
                return new ValidationResult("Invalid video format");
            }

            if (maxDurationSeconds > 0 && mediaInfo.Duration.TotalSeconds > maxDurationSeconds)
            {
                return new ValidationResult($"Video duration must not exceed {maxDurationSeconds} seconds");
            }
        }
        catch
        {
            return new ValidationResult("Invalid video format");
        }
        finally
        {
            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
        }

        return ValidationResult.Success;
    }
}
