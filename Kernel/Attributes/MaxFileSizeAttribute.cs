﻿using Microsoft.AspNetCore.Http;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;

namespace Kernel.Attributes;

public class MaxFileSizeAttribute(long maxFileSize) : ValidationAttribute
{
    private readonly long _maxFileSize = maxFileSize;

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
            return new ValidationResult("Something file must not be null");

        if (value is IFormFile file)
        {
            return ValidateFile(file);
        }
        else if (value is List<IFormFile> files)
        {
            ConcurrentBag<ValidationResult> validationResults = [];

            Parallel.ForEach(files, file =>
            {
                ValidationResult? result = ValidateFile(file);
                if (result != ValidationResult.Success && result != null)
                    validationResults.Add(result);
            });

            if (!validationResults.IsEmpty)
                return validationResults.FirstOrDefault();
        }

        return ValidationResult.Success;
    }

    private ValidationResult? ValidateFile(IFormFile file)
    {
        if (file?.Length > _maxFileSize)
            return new ValidationResult(ErrorMessage);

        return ValidationResult.Success;
    }
}
