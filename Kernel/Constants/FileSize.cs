﻿namespace Kernel.Constants;

public enum FileSize : long
{
    KB = 1024L,
    MB = 1024L * 1024L,
    GB = 1024L * 1024L * 1024L
}
