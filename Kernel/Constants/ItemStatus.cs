﻿namespace Kernel.Constants;

public enum ItemStatus
{
    OnlyTrash,
    OnlyNotTrash,
    All
}