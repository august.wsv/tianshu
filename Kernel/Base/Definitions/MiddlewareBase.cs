﻿using Kernel.Base.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Kernel.Base.Definitions;

public abstract class MiddlewareBase : IMiddlewareBase
{
    public abstract List<string> ApplyList { get; }

    public string GetPath(HttpContext context)
    {

        RouteData routeData = context.GetRouteData();
        string[]? templateSegments = (context.GetEndpoint() as RouteEndpoint)?.RoutePattern?.RawText?.Split('/');

        string? prefix = templateSegments?[0];
        object? controller = routeData.Values["controller"];
        object? action = routeData.Values["action"];
        string? dynamicSegment = templateSegments?[^1].ToString() != action?.ToString() ? templateSegments?[^1] : "";

        return $"{prefix}#{controller}@{action}[{dynamicSegment}]";
    }
}
