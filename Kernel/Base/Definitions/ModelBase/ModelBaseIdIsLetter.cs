﻿using System.ComponentModel.DataAnnotations;
using Kernel.Base.Contracts.IModelBase;

namespace Kernel.Base.Definitions.ModelBase;

public abstract class ModelBaseIdIsLetter : ModelBaseMain, IModelBaseIdIsLetter
{
    [Key]
    public string Id { get; set; }
}
