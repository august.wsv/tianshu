﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kernel.Base.Contracts.IModelBase;

namespace Kernel.Base.Definitions.ModelBase;

public abstract class ModelBaseIdIsNumber : ModelBaseMain, IModelBaseIdIsNumber
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }
}
