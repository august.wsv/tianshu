﻿using System.ComponentModel.DataAnnotations;
using Kernel.Base.Contracts.IModelBase;

namespace Kernel.Base.Definitions.ModelBase;

public abstract class ModelBaseMain : IModelBaseMain
{
    [Required]
    public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

    public DateTime? UpdatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }
}
