﻿using Kernel.Base.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Kernel.Base.Definitions;

public abstract class UnitOfWorkBase : IUnitOfWorkBase
{
    private readonly DbContext _context;

    public UnitOfWorkBase(DbContext context)
    {
        _context = context;
    }

    public async Task<bool> SaveChangesWithTransactionAsync(Func<Task> operation, CancellationToken cancellationToken = default)
    {
        using IDbContextTransaction transaction = await _context.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            await operation();
            int result = await _context.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);
            return result > 0;
        }
        catch (Exception ex)
        {
            await transaction.RollbackAsync();
            Console.WriteLine($"Error executing operation: {ex.Message}");
            return false;
        }
    }
}