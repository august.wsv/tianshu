﻿using Kernel.Base.Contracts.IModelBase;
using Kernel.Base.Contracts.IRepositoryBase;
using Kernel.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Kernel.Base.Definitions.RepositoryBase;

public abstract class RepositoryBaseIdIsNumber<T> : RepositoryBaseMain<T>, IRepositoryBaseIdIsNumber<T> where T : class, IModelBaseMain
{
    private readonly DbContext _context;
    private readonly DbSet<T> _dbSet;

    public RepositoryBaseIdIsNumber(DbContext context, IMemoryCache cache) : base(context, cache)
    {
        _context = context;
        _dbSet = _context.Set<T>();
    }

    #region Query
    public async Task<TResult?> GetObjectById<TResult>
    (
        long id,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    where TResult : IModelJustIdIsNumber
    {
        IQueryable<TResult> query = ApplyTResultQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.FirstOrDefaultAsync(entity => entity.Id.Equals(id));
    }
    #endregion
}