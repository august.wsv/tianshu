﻿using Kernel.Base.Contracts.IModelBase;
using Kernel.Base.Contracts.IRepositoryBase;
using Kernel.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Linq.Expressions;
using X.PagedList;

namespace Kernel.Base.Definitions.RepositoryBase;

public abstract class RepositoryBaseMain<T> : IRepositoryBaseMain<T> where T : class, IModelBaseMain
{
    private readonly DbContext _context;
    private readonly IMemoryCache _cache;
    private readonly DbSet<T> _dbSet;

    public RepositoryBaseMain(DbContext context, IMemoryCache cache)
    {
        _context = context;
        _cache = cache;
        _dbSet = _context.Set<T>();
    }

    #region Query
    public async Task<IPagedList<TResult>> GetPagedListCache<TResult>
    (
        string cacheKey,
        int page, int pageSize,
        Func<IQueryable<T>, IQueryable<TResult>>? querySelect = null,
        Func<IQueryable<TResult>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    where TResult : IModelJustDeletedAt
    {
        List<TResult>? cachedList = _cache.Get<List<TResult>>(cacheKey);
        if (cachedList is null)
        {
            IQueryable<T> query = _dbSet;
            IQueryable<TResult> sss = querySelect != null ? querySelect(query) : (IQueryable<TResult>)query;
            cachedList = await sss.ToListAsync();
            _cache.Set(
                key: cacheKey,
                value: cachedList,
                options: new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(5))
            );
        }
        IQueryable<TResult> cacheQuery = cachedList.AsQueryable();
        IQueryable<TResult> result = ApplyTResultQueryModifierII(cacheQuery, queryModifier, itemStatus);
        return await result.ToPagedListAsync(page, pageSize);
    }

    public async Task<IPagedList<TResult>> GetPagedList<TResult>
    (
        int page,
        int pageSize,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<TResult> query = ApplyTResultQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.ToPagedListAsync(page, pageSize);
    }

    public async Task<List<TResult>> GetList<TResult>
    (
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<TResult> query = ApplyTResultQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.ToListAsync();
    }

    public async Task<TResult?> GetObjectByField<TResult>
    (
        Expression<Func<TResult, bool>> predicate,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<TResult> query = ApplyTResultQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.FirstOrDefaultAsync(predicate);
    }

    public async Task<int> GetCount
    (
        Expression<Func<T, bool>> predicate,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<T> query = ApplyTQueryModifier(_dbSet, null, itemStatus);
        return await query.CountAsync(predicate);
    }

    public async Task<bool> SatisfyAny
    (
        Expression<Func<T, bool>> predicate,
        Func<IQueryable<T>, IQueryable<T>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<T> query = ApplyTQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.AnyAsync(predicate);
    }

    public async Task<bool> SatisfyAll
    (
        Expression<Func<T, bool>> predicate,
        Func<IQueryable<T>, IQueryable<T>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    {
        IQueryable<T> query = ApplyTQueryModifier(_dbSet, queryModifier, itemStatus);
        return await query.AllAsync(predicate);
    }
    #endregion

    #region Command
    public async Task Create(T entity, CancellationToken cancellationToken = default)
    {
        await _dbSet.AddAsync(entity, cancellationToken);
    }

    public async Task Create(List<T> entities, CancellationToken cancellationToken = default)
    {
        await _dbSet.AddRangeAsync(entities, cancellationToken);
    }

    public async Task Update(T entity)
    {
        entity.UpdatedAt = DateTime.UtcNow;
        _dbSet.Attach(entity);
        await Task.CompletedTask;
    }

    public async Task Update(List<T> entities)
    {
        entities.ForEach(entity => entity.UpdatedAt = DateTime.UtcNow);
        _dbSet.AttachRange(entities);
        await Task.CompletedTask;
    }

    public async Task Delete(T entity)
    {
        _dbSet.Remove(entity);
        await Task.CompletedTask;
    }

    public async Task Delete(List<T> entities)
    {
        _dbSet.RemoveRange(entities);
        await Task.CompletedTask;
    }

    public async Task SoftDelete(T entity)
    {
        entity.DeletedAt = DateTime.UtcNow;
        _dbSet.Update(entity);
        await Task.CompletedTask;
    }

    public async Task SoftDelete(List<T> entities)
    {
        entities.ForEach(entity => entity.DeletedAt = DateTime.UtcNow);
        _dbSet.UpdateRange(entities);
        await Task.CompletedTask;
    }

    public async Task Restore(T entity)
    {
        entity.DeletedAt = null;
        _dbSet.Update(entity);
        await Task.CompletedTask;
    }

    public async Task Restore(List<T> entities)
    {
        entities.ForEach(entity => entity.DeletedAt = null);
        _dbSet.UpdateRange(entities);
        await Task.CompletedTask;
    }
    #endregion

    protected IQueryable<TResult> ApplyTResultQueryModifier<TResult>(IQueryable<T> query, Func<IQueryable<T>, IQueryable<TResult>>? queryModifier, ItemStatus itemStatus)
    {
        switch (itemStatus)
        {
            case ItemStatus.OnlyNotTrash:
                query = query.Where(x => x.DeletedAt == null);
                break;
            case ItemStatus.OnlyTrash:
                query = query.Where(x => x.DeletedAt != null);
                break;
        }
        return queryModifier != null ? queryModifier(query) : (IQueryable<TResult>)query;
    }

    private IQueryable<TResult> ApplyTResultQueryModifierII<TResult>(IQueryable<TResult> query, Func<IQueryable<TResult>, IQueryable<TResult>>? queryModifier, ItemStatus itemStatus)
    where TResult : IModelJustDeletedAt
    {
        switch (itemStatus)
        {
            case ItemStatus.OnlyNotTrash:
                query = query.Where(x => x.DeletedAt == null);
                break;
            case ItemStatus.OnlyTrash:
                query = query.Where(x => x.DeletedAt != null);
                break;
        }
        return queryModifier != null ? queryModifier(query) : query;
    }

    private IQueryable<T> ApplyTQueryModifier(IQueryable<T> query, Func<IQueryable<T>, IQueryable<T>>? queryModifier, ItemStatus itemStatus)
    {
        switch (itemStatus)
        {
            case ItemStatus.OnlyNotTrash:
                query = query.Where(x => x.DeletedAt == null);
                break;
            case ItemStatus.OnlyTrash:
                query = query.Where(x => x.DeletedAt != null);
                break;
        }

        return queryModifier != null ? queryModifier(query) : query;
    }
}