using Microsoft.AspNetCore.Http;

namespace Kernel.Base.Contracts;

public interface IMiddlewareBase
{
    public abstract List<string> ApplyList { get; }
    protected string GetPath(HttpContext context);
}
