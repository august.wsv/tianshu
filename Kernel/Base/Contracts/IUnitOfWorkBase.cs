﻿namespace Kernel.Base.Contracts;

public interface IUnitOfWorkBase
{
    Task<bool> SaveChangesWithTransactionAsync(Func<Task> operation, CancellationToken cancellationToken = default);
}
