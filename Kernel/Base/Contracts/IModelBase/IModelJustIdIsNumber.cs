﻿namespace Kernel.Base.Contracts.IModelBase;

public interface IModelJustIdIsNumber
{
    public long Id { get; set; }
}
