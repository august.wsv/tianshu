﻿namespace Kernel.Base.Contracts.IModelBase;

public interface IModelJustIdIsLetter
{
    public string Id { get; set; }
}
