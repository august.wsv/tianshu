﻿namespace Kernel.Base.Contracts.IModelBase;

public interface IModelJustDeletedAt
{
    public DateTime? DeletedAt { get; set; }
}
