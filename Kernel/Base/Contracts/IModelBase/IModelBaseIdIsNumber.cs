﻿namespace Kernel.Base.Contracts.IModelBase;

public interface IModelBaseIdIsNumber : IModelBaseMain, IModelJustIdIsNumber { }
