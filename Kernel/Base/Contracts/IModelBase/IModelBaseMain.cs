﻿namespace Kernel.Base.Contracts.IModelBase;

public interface IModelBaseMain : IModelJustDeletedAt
{
    public DateTime CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }
}
