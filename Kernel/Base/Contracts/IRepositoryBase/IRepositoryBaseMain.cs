﻿using Kernel.Base.Contracts.IModelBase;
using Kernel.Constants;
using System.Linq.Expressions;
using X.PagedList;

namespace Kernel.Base.Contracts.IRepositoryBase;

public interface IRepositoryBaseMain<T> where T : class
{
    #region Query
    Task<IPagedList<TResult>> GetPagedListCache<TResult>
    (
        string cacheKey,
        int page, int pageSize,
        Func<IQueryable<T>, IQueryable<TResult>>? querySelect = null,
        Func<IQueryable<TResult>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    where TResult : IModelJustDeletedAt;

    Task<IPagedList<TResult>> GetPagedList<TResult>
    (
        int page,
        int pageSize,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    Task<List<TResult>> GetList<TResult>
    (
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    Task<TResult?> GetObjectByField<TResult>
    (
        Expression<Func<TResult, bool>> predicate,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    Task<int> GetCount
    (
        Expression<Func<T, bool>> predicate,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    Task<bool> SatisfyAny
    (
        Expression<Func<T, bool>> predicate,
        Func<IQueryable<T>, IQueryable<T>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    Task<bool> SatisfyAll
    (
        Expression<Func<T, bool>> predicate,
        Func<IQueryable<T>, IQueryable<T>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    );

    #endregion

    #region Command
    Task Create(T entity, CancellationToken cancellationToken = default);

    Task Create(List<T> entity, CancellationToken cancellationToken = default);

    Task Update(T entity);

    Task Update(List<T> entity);

    Task Delete(T entity);

    Task Delete(List<T> entity);

    Task SoftDelete(T entity);

    Task SoftDelete(List<T> entity);

    Task Restore(T entity);

    Task Restore(List<T> entity);
    #endregion
}
