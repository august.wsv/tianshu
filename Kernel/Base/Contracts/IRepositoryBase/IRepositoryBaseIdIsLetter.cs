﻿using Kernel.Base.Contracts.IModelBase;
using Kernel.Constants;

namespace Kernel.Base.Contracts.IRepositoryBase;

public interface IRepositoryBaseIdIsLetter<T> : IRepositoryBaseMain<T> where T : class
{
    #region Query

    Task<TResult?> GetObjectById<TResult>
    (
        string id,
        Func<IQueryable<T>, IQueryable<TResult>>? queryModifier = null,
        ItemStatus itemStatus = ItemStatus.OnlyNotTrash
    )
    where TResult : IModelJustIdIsLetter;

    #endregion
}
