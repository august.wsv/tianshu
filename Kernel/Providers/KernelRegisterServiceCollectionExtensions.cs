﻿using Kernel.Support.Contracts;
using Kernel.Support.Definitions;
using Microsoft.Extensions.DependencyInjection;

namespace Kernel.Providers;

public static class KernelRegisterServiceCollectionExtensions
{
    public static IServiceCollection AddKernelRegister(this IServiceCollection services)
    {
        services.AddScoped<IStatusCode, StatusCode>();
        services.AddSingleton<IHelper, Helper>();
        return services;
    }
}
