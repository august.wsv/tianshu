﻿namespace Kernel.UtilityClasses;

public class UploadResult
{
    public bool IsSusscess { get; set; } = true;
    public string? Path { get; set; }
}